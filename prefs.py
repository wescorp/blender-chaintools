#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for ChainTools Preferences"""
import bpy
# from . import ops_base #needed for default selection method, not working yet

# Thanks Russel Dias, Mark Amery
# https://stackoverflow.com/questions/5137497/find-current-directory-and-files-directory
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
#

class ChainToolsPreferences(bpy.types.AddonPreferences):
    bl_idname = __package__ #if in a submodule, use __package__, otherwise use __name__

    # Not used, stored here as a pattern for how to use this stuff
    # def FillPrefixList(self, context):
    #     enumList = []#[('____', 'NONE', 'NONE', 'NONE', 0)]
    #     import json
    #     with open(self.JSONprefix, 'r') as prefixesFile:
    #         prefixes = json.load(prefixesFile)
    #         for i, (prefix, data) in enumerate( prefixes.items() ):
    #             #data contains [Type, Description, Icon], Type not used here
    #             continueMe = False
    #             for enum in enumList:
    #                 if (data[1] in enum):
    #                     continueMe = True
    #                     continue
    #             if (continueMe):
    #                 continue
    #             #unique_identifier, name, description, icon, number 
    #             enumList.append( (prefix, prefix, data[1], data[2], i) )
    #     return enumList
    # def FillChiralIdentifiersList(self, context):
    #     enumList = []#[('____', 'NONE', 'NONE', 'NONE', 0)]
    #     import json
    #     with open(self.JSONchiral, 'r') as chiralFile:
    #         chi = json.load(chiralFile)
    #         i = 0
    #         for axis, identifiers in chi.items() :
    #             if (axis == 'R'):
    #                 description = "Right"
    #             if (axis == 'L'):
    #                 description = "Left"
    #             if (axis == 'T'):
    #                 description = "Top"
    #             if (axis == 'B'):
    #                 description = "Bottom"
    #             if (axis == 'F'):
    #                 description = "Front"
    #             if (axis == 'B'):
    #                 description = "Back"
    #             if (axis == 'C'):
    #                 description = "Center"
    #             for identifier in identifiers:
    #                 enumList.append( (identifier, identifier, description, i) )
    #     return enumList
    # def FillSeperatorList(self, context):
    #     enumList = []#[('____', 'NONE', 'NONE', 'NONE', 0)]
    #     import json
    #     with open(self.JSONseperator, 'r') as sepFile:
    #         sep = json.load(sepFile)
    #         for i, (purpose, char) in enumerate(sep.items()):
    #             enumList.append( (purpose, char, purpose + " " + char, i ) )
    #     return enumList

    JSONprefix: bpy.props.StringProperty(
        name = "Prefix code file",
        subtype = 'FILE_PATH',
        default = dir_path + '/preferences/prefix.json',)
    JSONchiral: bpy.props.StringProperty(
        name = "Chiral Identifier file",
        subtype = 'FILE_PATH',
        default = dir_path + '/preferences/chiral_identifier.json',)
    JSONseperator:bpy.props.StringProperty(
        name = "Seperator file",
        subtype = 'FILE_PATH',
        default = dir_path + '/preferences/seperator.json',) 
    checkLayers: bpy.props.BoolProperty(
        name="Check armature layer when selecting chains: ",
        description = ("Disabled by default, because ChainTools already ignores "
                        "bones in invisible layers. Enable this if you want ChainTools "
                        "to not select chains when the bones have seperate layer-masks, "
                        "even if the layers are all currently visible."),
        default=False, )
    # defaultSelectionMethod: bpy.props.EnumProperty(
    #                         items = ops_base.ChainToolsOperator.select_method_enum_flag,
    #                         name = "Selection Method",
    #                         options = {'ENUM_FLAG'},
    #                         default = {'NAME', 'LOCATION'})
    # disabled for now, it isn't working
    
    # Not used, stored here as a pattern for how to use this stuff
    # prefixes:bpy.props.EnumProperty(
    #     items= FillPrefixList,
    #     name= "Prefixes",
    #     description = "List of Prefixes known to ChainTools",)
    # chiral_identifiers:bpy.props.EnumProperty(
    #     items= FillChiralIdentifiersList,
    #     name= "Chiral Identifiers",
    #     description = "List of Chiral Identifiers known to ChainTools",)
    # seperators:bpy.props.EnumProperty(
    #     items= FillSeperatorList,
    #     name= "Seperator Characters",
    #     description = "List of Seperator Characters known to ChainTools",)

    
    def draw(self, context):
        layout = self.layout
        layout.label(text="Chain Tools Preferences")
        layout.prop(self, "JSONprefix", text="Prefix JSON file", icon='FILE_TEXT')
        layout.prop(self, "JSONchiral", text="Chiral Identifier JSON file", icon='FILE_TEXT')
        layout.prop(self, "JSONseperator", text="Seperator Character JSON file", icon='FILE_TEXT')
        layout.prop(self, "checkLayers")
        # layout.prop(self, "defaultSelectionMethod", text="Default Selection Method")
