#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
import bpy
# import mathutils
# import math
# from operator import attrgetter
# from . import hierarchies
# from . import operator

#chainSelMethods =   [('NAME',"By Name", "Select all bones sharing basename- this will not detect branches", 0), 
#                     ('CONNECTED',"Connected Parents", "Select connected parents/children", 1), 
#                     ('DISCONNECTED',"Disconnected Parents", "Select disconnected parents/children that share head/tail location", 2),
#                     ('PARENT',"Any Parents", "Select any parents/children that shares head/tail location", 3), 
#                     ('LOCATION',"By Location", "Select bones sharing head/tail location, regardless of parents", 4),
#                     ('CONSTRAINT',"Similar Constraints", "Select bones with similarly-named constraint targets", 5),
#                     ('NAMELOC', "Name and Location", "Select bones sharing head/tail location and basename", 6)
#                    ],

#TODO: use the invoke function to set some properties
# to improve performance
# not high priority, performance is already pretty good!
#TODO: use class inheritance

#get the class list or whatever

class ChainToolsOperator(bpy.types.Operator):
    """ChainTools Operator Base Class"""
    select_method_enum_flag=[('NAME'        , "By Name"             , "Select bones with incremental names- this will not detect branches", 1), 
                             ('CONNECTED'   , "Connected Parents"   , "Select connected parents/children"                                 , 2), 
                             ('DISCONNECTED', "Disconnected Parents", "Select disconnected parents/children that share head/tail location", 4),
                             ('LOCATION'    , "By Location"         , "Select bones sharing head/tail location"                           , 8),
                             ('CONSTRAINT'  , "Similar Constraints" , "Select bones with similarly-named constraint targets"              , 16),]
    # the following is DEPRECATED, use enum_flag instead ^
    # select_method_enum=[('NAME'        , "By Name"             , "Select bones with incremental names- this will not detect branches", 0), 
    #                     ('CONNECTED'   , "Connected Parents"   , "Select connected parents/children"                                 , 1), 
    #                     ('DISCONNECTED', "Disconnected Parents", "Select disconnected parents/children that share head/tail location", 2),
    #                     ('PARENT'      , "Any Parents"         , "Select any parents/children that shares head/tail location"        , 3), 
    #                     ('LOCATION'    , "By Location"         , "Select bones sharing head/tail location"                           , 4),
    #                     ('CONSTRAINT'  , "Similar Constraints" , "Select bones with similarly-named constraint targets"              , 5),
    #                     ('NAMELOC'     , "Name and Location"   , "Select bones sharing head/tail location and incremental names"     , 6),]
    symmetry_enum: [('X', "X-Axis", "Symmetry along X-Axis", 0),
                     ('Y', "Y-Axis", "Symmetry along Y-Axis", 1),
                     ('Z', "Z-Axis", "Symmetry along Z-Axis", 2),
                     ('NONE', "None", "Do not name symmetrically", 3),]
    # bl_idname = "object.chain"
    # bl_label = "Rename Bone Chain"
    # bl_options = {'REGISTER', 'UNDO'}
    # select_method: bpy.props.EnumProperty(
    #                         items = [('NAME',"By Name", "Select bones with incremental names- this will not detect branches", 0), 
    #                                  ('CONNECTED',"Connected Parents", "Select connected parents/children", 1), 
    #                                  ('DISCONNECTED',"Disconnected Parents", "Select disconnected parents/children that share head/tail location"),
    #                                  ('PARENT',"Any Parents", "Select any parents/children that shares head/tail location", 3), 
    #                                  ('LOCATION',"By Location", "Select bones sharing head/tail location, regardless of parents and names", 4),
    #                                  ('CONSTRAINT',"Similar Constraints", "Select bones with similarly-named constraint targets", 5),
    #                                  ('NAMELOC', "Name and Location", "Select bones sharing head/tail location and incremental names", 6),
    #                                 ],
    #                         name = "Selection Method",
    #                         default = 'NAMELOC')
    # symmetry: bpy.props.EnumProperty(
    #                         items = [
    #                             ('X', "X-Axis", "Symmetry along X-Axis", 0),
    #                             ('Y', "Y-Axis", "Symmetry along Y-Axis", 1),
    #                             ('Z', "Z-Axis", "Symmetry along Z-Axis", 2),
    #                             ('NONE', "None", "Do not name symmetrically", 3),
    #                                 ],
    #                         name = "Use Symmetry",
    #                         description = " Add chiral identifier to end of name and rename symmetrical bones, if they exist",
    #                         default = 'X',)

# class MyOperator(ChainToolsOperator):
#     bl_idname = "object.cuss"
#     bl_label = "MyOperator"
#     bl_options = {'REGISTER', 'UNDO'}
#     def execute(self, context):
#         self.report(type = {"WARNING"}, message = "Running operator")
#         return {"FINISHED"}