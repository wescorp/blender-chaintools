#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for curve geometry functions""" 
import bpy
from . import f_mesh
from . import f_gen


def CalcLengthCrv(crv):
    wiresLength = []
    m = bpy.data.meshes.new_from_object(crv)
    wires = f_mesh.DetectWireEdges(m)
    for wire in wires:
        wiresLength.append(sum(f_mesh.WireMeshEdgeLengths(m, wire)))
    return sum(wiresLength)

def PointsFromCurve(obCrv, factorsList):
    """Returns a point from a u-value along a curve"""
    #for circular curve, should also include last vertex
    m = bpy.data.meshes.new_from_object(obCrv)
    pointsList = f_mesh.PointsFromWireMesh(m, factorsList, obCrv.location)
    bpy.data.meshes.remove(m)
    return pointsList

def DataFromRibbon(obCrv, factorsList, fReport=None):
    import time
    start = time.time()
    """Returns a point from a u-value along a curve"""
    rM = bpy.data.meshes.new_from_object(obCrv)
    ribbons = f_mesh.DetectRibbons(rM, fReport= fReport)
    for ribbon in ribbons:
        # could be improved, this will do a rotation for every ribbon
        # if even one is a circle
        if (ribbon[2]) == True:
            # could be a better implementation
            dupeCrv = obCrv.copy()
            dupeCrv.data = obCrv.data.copy()
            dupeCrv.data.extrude = 0
            dupeCrv.data.bevel_depth = 0 
            wM = bpy.data.meshes.new_from_object(dupeCrv)
            wires = f_mesh.DetectWireEdges(wM)
            bpy.data.curves.remove(dupeCrv.data) #removes the object, too
            ribbonsNew = []
            for ribbon, wire in zip(ribbons, wires):
                if (ribbon[2] == True): #if it's a circle
                    rNew = f_mesh.RotateRibbonToMatchWire(ribbon, rM, wire, wM)
                else:
                    rNew = ribbon
                ribbonsNew.append( rNew )
            ribbons = ribbonsNew
            break
    data = f_mesh.DataFromRibbon(rM, factorsList, obCrv.location, ribbons=ribbons, fReport=fReport)
    bpy.data.meshes.remove(rM)
    print ("time elapsed: ", time.time() - start)
    return data

def FindNearestPointOnCurve(obCrv, pointsList):
    m = bpy.data.meshes.new_from_object(obCrv)
    factorsList = f_mesh.FindNearestPointOnWireMesh(m, pointsList)
    bpy.data.meshes.remove(m)
    return factorsList


def SplineFromPoints(obCrv, splType, points):
    dataCrv = obCrv.data
    spl = dataCrv.splines.new(splType)
    if (splType == 'BEZIER'):
        spl.bezier_points.add(len(points) - 1)
        for p, bp in zip(points,spl.bezier_points):
            bp.co = p
            bp.handle_left_type  = 'AUTO'
            bp.handle_right_type = 'AUTO'
    else: # ( (splType == 'NURBS') or (splType == 'POLY') ):
        spl.points.add(len(points) - 1)
        for p, cv in zip(points, spl.points):
             cv.co = p.to_4d()
    return spl

def SmoothSpline(obCrv, splIndex, factor):
    #Creates weird stairstepping in some curves
    #maybe should have an option to pin endpoints if not a circle?
    spl = obCrv.data.splines[splIndex]
    circle = spl.use_cyclic_u
    if (spl.type == 'BEZIER'):
        bPoints = [bp for bp in spl.bezier_points]
        splength = len(bPoints) -1
        bPointTuples = []
        bPointNew = []
        for i, bp in enumerate(bPoints):
            #construct tuples of point, prev point, next point
            bpNext = bPoints[f_gen.cap(i+1, splength)]
            bpPrev = bPoints[f_gen.capMin(i-1, 0)]
            bPointTuples.append( (bp, bpPrev, bpNext) )
        #now fix the first/last if circle is True
        if (circle == True):
            bPointTuples[0]  = (bPoints[0], bPoints[-1], bPoints[1])
            bPointTuples[-1] = (bPoints[-1], bPoints[-2], bPoints[0])
        for tup in bPointTuples:
            bPointNew.append (SmoothBezierPoint(tup[0], tup[1], tup[2], factor))
        for i, bp in enumerate(bPoints):
            bp.co           = bPointNew[i][0]
            bp.handle_left  = bPointNew[i][1]
            bp.handle_right = bPointNew[i][2]
    else:
        points = [p for p in spl.points]
        splength = len(points) -1
        pointTuples = []
        pointNew = []
        for i, p in enumerate(points):
            #construct tuples of point, prev point, next point
            pNext = points[f_gen.cap(i+1, splength)]
            pPrev = points[f_gen.capMin(i-1, 0)]
            pointTuples.append( (p, pPrev, pNext) )
        #now fix the first/last if circle is True
        if (circle == True):
            pointTuples[0]  = (points[0], points[-1], points[1])
            pointTuples[-1] = (points[-1], points[-2], points[0])
            # expects no dupe points
        for tup in pointTuples:
            pointNew.append (SmoothPoint(tup[0].co.to_3d(), 
                        tup[1].co.to_3d(), tup[2].co.to_3d(), factor))
        for i, p in enumerate(points):
            if (circle):
                p.co = pointNew[i].to_4d()
            else:
                if ((i>0) and (i < (len(points) - 2 ) ) ):
                    p.co = pointNew[i].to_4d()

def SmoothPoint(p, pPrev, pNext, factor): #TODO rename
    #nurbs points are 4D, make sure to cast to 3D in the calling block
    #p, pPrev and pNext should just be vectors
    #factor must be between zero and one
    #this way I don't need to care if I'm getting a vertex or a NURBS point
    #will handle bezier points separately
    #returns the coord of the point after averaging

    #This is a Python port of Blender's internal code for curve smooth operator
    #except it interpolates the vector instead of doing it component-wise
    vecOld = p.copy()
    vecNew = p.copy()
    vecNew = vecNew.lerp((0.5 * pPrev), 1) +vecNew.lerp((0.5 * pNext), 1)
    offset = (vecOld * (1 - factor)) + (vecNew * factor) - vecOld
    return (vecOld + offset) #(weird, but should work)

def SmoothBezierPoint(bp, bpPrev, bpNext, factor):
    #the above, except for bezier points
    #This time, the input should be a bezier point, so i can access its handles
    #returns a tuple of coords
    vecOld = bp.co.copy()
    vecNew = bp.co.copy()
    vecNew = vecNew.lerp((0.5 * bpPrev.co), 1) +vecNew.lerp((0.5 * bpNext.co), 1)
    offset = (vecOld * (1 - factor)) + (vecNew * factor) - vecOld

    bpNew = bp.co.copy()
    bpNewLeft = bp.handle_left.copy()
    bpNewRight = bp.handle_right.copy()
    bpNew      += offset
    bpNewLeft  += offset
    bpNewRight += offset
    return (bpNew, bpNewLeft, bpNewRight)

def EnsureCurveIsRibbon(crv, defaultRadius = 0.1):
    crvRadius = 0
    if (crv.data.bevel_depth == 0):
        crvRadius = crv.data.extrude
    else: #Set ribbon from bevel depth
        crvRadius = crv.data.bevel_depth
        crv.data.bevel_depth = 0
        crv.data.extrude = crvRadius
    if (crvRadius == 0):
        crv.data.extrude = defaultRadius

# I'd like to write a function for interpolating data from the length of a curve
# This will be tricky: BEZIER curves always have a vertex at the same location
#   as the control point after conversion to mesh, but NURBS almost never do.
#   So there may be two ways of getting the data for NURBS:
#   finding the nearest 2 cv's to each vertex and interpolating data from them
#     (will fail in edge cases, since a vertex may be closer to a cv it's not
#      supposed to be getting data from-- unconnected cv's)
#   converting to BEZIER temporarily, then doing the interpolation from the
#     vertices on the mesh that pair up with a bp.
# gonna leave this as a TODO for the moment.
#
# I'm gonna use ribbon meshes for figuring out curve twist, since it isn't
#  easily accesible from Python and would require a lot of calculation.
# Ribbon meshes have two 'lines' of vertices, the top edge of the ribbon
#  and the bottom edge, which have even and odd numbered vertex indices,
#  respectively. Between the lines are "ladder rungs" which are edges between
#  an even vertex and the next vertex (which is odd). There are faces between
#  every two ladder rungs, so (0,1,2,3), (4,5,6,7), (8,9,10,11) are faces
#  0, 4, and 8 are the top-left corners, 1, 5, and 9 are the bottom left,
#  etc. This way, I can 'walk' the ribbon as if it were a wire mesh.
# The normal along the ribbon will give me the twist in absolute terms
#  which can then be used to solve bone roll, and the length of the edge will
#  give me the curve radius at that point, again, in absolute measurements.