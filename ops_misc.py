#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for Miscelaneous operators"""
import bpy
from . import ops_base
from . import f_name
from . import f_editBone

# testing
from . import f_mesh


class RandomizeNames(ops_base.ChainToolsOperator):
    """Randomize names"""
    bl_idname = "object.randomize_names"
    bl_label = "Randomize names"
    bl_options = {'REGISTER', 'UNDO'}
    @classmethod
    def poll(cls, context):
        return  ( (context.mode == 'EDIT_ARMATURE') or (context.mode == 'POSE') or 
                    (context.mode == 'OBJECT') )

    def execute(self, context):
        if ( (context.mode == 'EDIT_ARMATURE') or (context.mode == 'POSE') ):
            bones = context.selected_editable_bones
            for bone in bones:
                bone.name = f_name.randomString()
            return {"FINISHED"}
        elif (context.mode == 'OBJECT'):
            obs = context.selected_objects
            for ob in obs:
                ob.name = f_name.randomString()
            return {"FINISHED"}

class TestOperator(ops_base.ChainToolsOperator):
    """Test Operator"""
    bl_idname = "object.randomize_names"
    bl_label = "Test Operator"
    bl_options = {'REGISTER', 'UNDO'}

    # @classmethod
    # def poll(cls, context):
    #     return  ( (context.mode == 'EDIT_ARMATURE') or (context.mode == 'POSE') or 
    #                 (context.mode == 'OBJECT') )

    def execute(self, context):
        obAct = bpy.context.active_object
        # # try:
        m = bpy.data.meshes.new_from_object(obAct)
        factors = [0,0.25,0.5,0.75,1]
        factorsList = [factors for i in range(len(obAct.data.splines))]
        dataLists = f_mesh.DataFromRibbon(m, factorsList, obAct.location, self.report)
        for (points, widths, normals) in dataLists:
            for p, w, n in zip(points, widths, normals):
                print ("Point: ", p); print ("Width: ", w); print ("Normal: ", n)
                empty = bpy.data.objects.new("empty", None)
                empty.location = p; empty.empty_display_size = w
                #works for bones with align_roll() so no need to figure out the math
                context.collection.objects.link(empty)
        # f_mesh.DetectRibbonEdges(m, self.report)
        # except:
        #     print ("Select a curve")
        #     return {'CANCELLED'}
        return {"FINISHED"}

class CreateBonesFromCurve(ops_base.ChainToolsOperator):
    """Create Armature from Selected Curve"""
    bl_idname = "object.create_bones_along_curve"
    bl_label = "Create Armature From Selected Curve"
    bl_options = {'REGISTER', 'UNDO'}

    #TODO: additional function for object mode, add bones to active armature
    #       along selected curve
    #TODO: option to use spline radius as envelope radius
    #TODO: option to use spline tilt to set bone roll
    #TODO: option to add symmetrically
    #      but mirror modifier should do this. My addon should check for that and
    #      name appropriately

    def populate_curves_list(self, context):
        #eventually add curve icon
        retList = []
        curves = []
        i = 0
        for ob in context.selected_objects:
            #this way, selected objects are higher on the list
            if (ob.type == 'CURVE'):
                if (len (ob.data.splines) > 0):
                    data_add = (ob.name, ob.name, "Curve to Create Bones Along", i)
                    retList.append(data_add)
                    curves.append(ob.name)
                    i+=1
        for ob in bpy.data.objects:
            if ((ob.type == 'CURVE') and (ob.name not in curves)):
                if (len (ob.data.splines) > 0):
                    data_add = (ob.name, ob.name, "Curve to Create Bones Along", i)
                    retList.append(data_add)
                    i+=1
        if (i == 0):
            retList = [(None, None, None, 0)]
        return(retList)
        #COPIED FROM ABOVE, DE-DUPLICATE TODO TODO HACK HACK
        # I think I can do this by defining a subclass and inheriting.

    @classmethod
    def poll(cls, context):
        if (context.active_object):
            return ((context.active_object.type == 'CURVE') and
                    (context.mode == 'OBJECT') )
        return False

    quantity: bpy.props.IntProperty(
                name="Bones Per Spline",
                description="Number of bones to add along each spline; 0 creates bones on Spline control points",
                default = 0,
                min = 0, # 0 is fit to spline points
                soft_max = 64,
                subtype='UNSIGNED')
    name: bpy.props.StringProperty(
                name="Name",
                description="Name of the bone chain; each bone will use this name and a number (or multiple numbers, if the curve contains multiple splines).",
                default = "BoneChain")
                #Shouldn't this take the curve's name by default?
    is_connected: bpy.props.BoolProperty(
                name="Connected Parents",
                description="Whether or not to connect each bone to the previous bone, or with disconnected parents.",
                default = True) #TODO this can be an ENUM property that gives more options
    parentBranch: bpy.props.BoolProperty(
                name="Parent Branches",
                description="Whether or not to create parents at branch points",
                default = True)
    assignRig: bpy.props.BoolProperty(
                name="Assign Rig To Curve",
                description="Assign the newly created rig to the curve using an Armature Deform Modifier.",
                default = False)
    
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def execute(self, context):
        import time
        start = time.time()
        print ("generating armature...")
        obCrv = context.active_object
        try:
            obCrv.data.splines #just check that it's a curve
        except AttributeError:
            self.report(type={"ERROR_INVALID_INPUT"}, message="Select a curve object.")
            return {"FINISHED"}
        #Create new Armature;
        bpy.ops.object.armature_add()
        obArm = context.active_object
        obArm.location = (0,0,0,)
        obArm.name = self.name
        # Remove default bone
        bpy.ops.object.mode_set(mode='EDIT')
        bones = context.active_object.data.edit_bones
        bones.remove(bones[0])
        f_editBone.BonesFromCurve(
            obArm,
            obCrv,
            quantity=self.quantity,
            connected=self.is_connected,
            parentBranch=self.parentBranch,
            baseName=self.name,
            fReport=self.report)
        bpy.ops.object.mode_set(mode='OBJECT')
        if (self.assignRig):
            obCrv.parent = obArm
            m = obCrv.modifiers.new(obArm.name + "_Armature_Deform", "ARMATURE")
            m.use_apply_on_spline = True
            m.use_bone_envelopes = True
            m.use_vertex_groups = False
            m.object = obArm
            # for b in obArm.data.bones:
            #     b.envelope_distance = 0.001 #ensures that each bone only affects the current point
            #     b.head_radius = 0.001 #ensures that each bone only affects the current point
            #     b.tail_radius = 0.001 #ensures that each bone only affects the current point
        print ("operation finished in", time.time() - start)
        return {"FINISHED"}

