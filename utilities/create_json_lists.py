#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for creating JSON lists used by ChainTools Preferences"""

chi = {
    'R':['R', 'r', 'right', 'Right', 'RIGHT',],
    'L':['L', 'l', 'left' , 'Left',  'LEFT' ,],
    'F':['front', 'Front', 'FRONT', 'Fr', 'fr', 'FR',],
    'B':['back' , 'Back' , 'BACK' , 'Bk', 'bk', 'BK',],
    'U':['top', 'Top', 'TOP',],
    'D':['bot', 'Bot', 'BOT',],# 'bottom', 'BOTTOM'],
    'C':['Center', 'CENTER', 'c', 'C', 'cen', 'CEN']
}

# chiOpposite = (['.R', '.L'], ['_R', '_L'], ['-R', '-L'], [' R', ' L'], 
#                ['.r', '.l'], ['_r', '_l'], ['-r', '-l'], [' r', ' l'],
#                ['.right', '.left'], ['_right', '_left'], ['-right', '-left'],
#                [' right', ' left'], ['right', 'left'], ['.Right', '.Left'], 
#                ['_Right', '_Left'], ['-Right', '-Left'], [' Right', ' Left'],
#                ['Right', 'Left'], ['.RIGHT', '.LEFT'], ['_RIGHT', '_LEFT'],
#                ['-RIGHT', '-LEFT'], [' RIGHT', ' LEFT'], ['RIGHT', 'LEFT'],
#                ['.front', '.back'], ['.Front', '.Back'], ['.FRONT', '.BACK'],
#                ['.fr', '.bk'], ['.Fr', '.Bk'], ['.FR', '.BK'],
#                ['.top', '.bot'], ['.Top', '.Bot'], ['.TOP', '.BOT'])
#Do I need a JSON for this?

######## PREFIXES ########
# Syntax: "CODE": ['DATA_TYPE', 'DESCRIPTION', 'ICON']
#Data types used by ChainTools:
# ARMATURE
# BONE
# more to come
# If two prefixes use the same description, they'll be considered interchangable
#  and the first item in the dict will be used


#Four characters; these are my preferred conventions and a bunch of extras
prefixes = {
    "NONE": ["NONE", "NONE", "NONE"],
    "MECH": ['BONE', "Mechanism Bone", 'BONE_DATA'],
    "DFRM": ['BONE', "Deform Bone", 'BONE_DATA'],
    # "SKIN": ['BONE', "Deform Bone", 'BONE_DATA'],
    "OPTN": ['BONE', "Animation Properties Bone", 'BONE_DATA'],
    "CTRL": ['BONE', "Animation Control Bone", 'BONE_DATA'],
    "VISL": ['BONE', "Visualization (UI) Bone", 'BONE_DATA'],
    # "UIUX": ['BONE', "Visualization (UI) Bone", 'BONE_DATA'],
    "META": ['BONE', "Meta (re-proportioning) Bone", 'BONE_DATA'],
    "ROOT": ['BONE', "Root Bone", 'BONE_DATA'],
    "SCKT": ['BONE', "Socket Bone", 'BONE_DATA'],
    "PRNT": ['BONE', "Parent Bone", 'BONE_DATA'],
    "POLL": ['BONE', "IK Poll Vector Bone", 'BONE_DATA'],
    "DMMY": ['BONE', "Dummy Bone", 'BONE_DATA'],
    "SIML": ['BONE', "Simulation Bone", 'BONE_DATA'],
    # "DUMM": ['BONE', "Dummy Bone", 'BONE_DATA'],
    # "SWCH": ['BONE', "reserved for future use", 'BONE_DATA'], #SWCH
    # "ORIG": "reserved for future use", #original
    # "COPY": "reserved for future use", #copier
    # "FROM": "reserved for future use", #copy from me
    # "DUPE": "reserved for future use", #duplicate/dummy
    # "HIDE": "reserved for future use", #hidden
    # "HLDR": "reserved for future use", #holder
    # "SCFD": "reserved for future use", #scaffold
    # "PANL": "reserved for future use", #panel
    # "NULL": "reserved for future use", #Null
    # "PASS": "reserved for future use", #Pass
    # "CVRT": "reserved for future use", #convert
    # "LOCL": "reserved for future use", #local
    # "WRLD": "reserved for future use", #world
    # "BASE": "reserved for future use", #base
    # "BSIS": "reserved for future use", #basis
    # "MTRX": "reserved for future use", #matrix
    # "XFRM": "reserved for future use", #transform
    # "LOCN": "reserved for future use", #location
    # "ROTN": "reserved for future use", #rotation
    # "ORNT": "reserved for future use", #orientation
    # "SCLE": "reserved for future use", #scale
    # "LEFT": "reserved for those too stupid to see the disadvantages of sorting symmetry this way",
    # "RGHT": "reserved for those too stupid to see the disadvantages of sorting symmetry this way",
    "HELP": ['OBJECT',  "Helper Object", 'OBJECT_DATA'],
    # "HLPR": ['OBJECT',  "Helper Object", 'OBJECT_DATA'],
    "ARMT": ['OBJECT',  "Armature Object", 'OBJECT_DATA'],
    # "RIGG": ['OBJECT',  "Armature Object", 'OBJECT_DATA'],
    "GPEN": ['GREASEPENCIL',  "Grease Pencil Object", 'GREASEPENCIL'],
    # "GPCL": ['GREASEPENCIL',  "Grease Pencil Object", 'GREASEPENCIL'],
    # "DRAW": ['GREASEPENCIL',  "Grease Pencil Object", 'GREASEPENCIL'],
    "CHAR": ['COLLECTION',  "Character Collection", 'GROUP'],
    "GEOM": ['OBJECT',  "Geometry Object", 'OBJECT_DATA'],
    # "MESH": ['MESH',  "Mesh Datablock", 'MESH_DATA'],
    # "CURV": ['CURVE',  "Curve Datablock", 'MESH_DATA'],
    # "SURF": ['NURBS',  "NURBS Surface Datablock", 'SURFACE_DATA'],
    # "TEXT": ['TEXT',  "Text Datablock", 'TEXT'],
    # "MBLL": ['META',  "MetaBall Datablock", 'META_BALL'],
    # "BALL": ['META',  "MetaBall Datablock", 'META_BALL'],
    # "LGHT": ['LIGHT',  "LIGHT OBJECT", 'LIGHT'],
    # "ENVT": "reserved for future use",
    # "PROB": "reserved for future use",
    # "CMRA": "reserved for future use",
    # "CAMR": "reserved for future use", #alternative to CMRA
    # "SPKR": "reserved for future use",
    # "FRCE": "reserved for future use",
}
#TODO: make Rigify compatible prefix list
#TODO: make Blender Institute compatible prefix list

PreferredSeperators = {'PREFIX':'-', 'NUMBER':'.', 'CHIRAL_IDENTIFIER':'.',}

if __name__ == "__main__":
    #Windows:
    # import json
    # with open('preferences\\prefix.json', 'w') as outfile:
    #     json.dump(prefixes, outfile, sort_keys=False, indent=4, separators=(',', ': '))
    # with open('preferences\\chiral_identifier.json', 'w') as outfile:
    #     json.dump(chi, outfile, sort_keys=False, indent=4, separators=(',', ': '))
    # with open('preferences\\seperator.json', 'w') as outfile:
    #     json.dump(PreferredSeperators, outfile, sort_keys=False, indent=4, separators=(',', ': '))
    
    #Linux
    import json
    with open('preferences/prefix.json', 'w') as outfile:
        json.dump(prefixes, outfile, sort_keys=False, indent=4, separators=(',', ': '))
    with open('preferences/chiral_identifier.json', 'w') as outfile:
        json.dump(chi, outfile, sort_keys=False, indent=4, separators=(',', ': '))
    with open('preferences/seperator.json', 'w') as outfile:
        json.dump(PreferredSeperators, outfile, sort_keys=False, indent=4, separators=(',', ': '))