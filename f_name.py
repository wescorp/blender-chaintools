#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for managing names"""
import bpy
import string
import random
import json

####################################################################################
# Search Keys
####################################################################################

    
def GetDictJSON(fileProp = "JSONprefix"):
    #fileProp is stored in ChainTools Preferences
    addonPrefs = bpy.context.preferences.addons[__package__].preferences
    try:
        filepath = getattr(addonPrefs, fileProp)
    except AttributeError:
        #this happens when ChainTools is loading and doesn't have preferences yet.
        return {"NONE": ["NONE", "NONE", "NONE"],}
    with open(filepath, 'r') as fileOb:
        return json.load(fileOb)

prefixes = GetDictJSON(fileProp="JSONprefix")

def GetSeperator(sepType="Prefix"):
    seps = GetDictJSON(fileProp="JSONseperator")
    try:
        return seps[sepType]
    except:
        return ""

####################################################################################

######## PREFIXES ########
def DetectPrefix(s):
    """Detects prefix in string
       Returns the prefix, if found, else None"""
    for prefix in prefixes.keys():
            if (s.startswith(prefix, 0, len(prefix)) == True):
                return prefix #This leaves separator characters to the other functions
    return None

def HasPrefix(s):
    """Determines if string has a prefix
       Returns True if foun., else False"""
    for prefix in prefixes.keys():
            if (s.startswith(prefix, end=len(prefix)) == True):
                return True #This leaves separator characters to the other functions
    return False

def StripPrefix(s):
    """Removes prefix from string
       Returns s without prefix, if found. else s"""
    prefix = DetectPrefix(s)
    if (prefix is not None):
        return s[len(prefix) + 1:]
    return s

def ComparePrefix(s1, s2):
    """Compares prefixes in two strings
       ReturnsTrue if they are the same, else False"""
    return DetectPrefix(s1) == DetectPrefix(s2)

######## CHIRAL IDENTIFIERS ########
def DetectChiral(s):
    """Detects right or left handed-ness of name
       Returns a tuple containing chiral string, the number 
       of chiral identifiers detected, and a copy of the 
       input string with chiral identifiers removed. 
       Otherwise, and empty string, False, and the original
        name are returned."""
    #BAD BEHAVIOR WHEN CHIRAL IDENTIFIER IS NOT ON END OF STRING
    #look in the end of the string if it's long enough
    #The function won't look in the middle of the string. Is this good?
    #YES, for people that follow my convention of putting chirality at the end of the name
    #some will want to put numbers after chirality
    chiral_identifiers = GetDictJSON(fileProp="JSONchiral")
    chiStr = ''
    chiSep = GetSeperator("CHIRAL_IDENTIFIER")
    foundChi = 0
    removedChi = s
    n = -6 if (len(s) >= 6) else 0 
    for (axis, identifiers) in chiral_identifiers.items():
        for x in identifiers:
            findMe = chiSep + x
            # print (findMe)
            # print (s)
            if ( s.find(findMe, (n) ) != -1):
                chiStr = findMe 
                foundChi += 1
                spl = removedChi.split(findMe)
                removedChi = ""
                for ind in range(len(spl)):
                    removedChi += spl[ind]
                #this should add together all of the substrings from
                # the split exceot the last one
                # which I want to remove
    #it's still possible more than one can be found, if for example .L.R occurs
    return (chiStr, foundChi, removedChi) #TODO split into the individual functions, as above
    # I have no idea if this still works, need to test and maybe re-write?

def compareChiral(a, b):
    chi_a, hasChi_a, notUsed = DetectChiral(a)
    chi_b, hasChi_b, notUsed = DetectChiral(b)
    
    if (hasChi_a != hasChi_b):
        return False

    if (('l' in chi_a) or ('L' in chi_a)):
        chi_a = 'LEFT'
    if (('r' in chi_a) or ('R' in chi_a)):
        chi_a = 'RIGHT'
        
    if (('l' in chi_b) or ('L' in chi_b)):
        chi_b = 'LEFT'
    if (('r' in chi_b) or ('R' in chi_b)):
        chi_b = 'RIGHT'
    
    #Re-Write to check chiral_identifiers axis

    return (chi_a == chi_b)

def GetChiOpposite(chi):
    chiral_identifiers = GetDictJSON(fileProp="JSONchiral")
    chiSep = GetSeperator("CHIRAL_IDENTIFIER")
    for (axis, identifiers) in (chiral_identifiers.items()):
        for i, x in enumerate(identifiers):
            if ((chiSep + x) == chi):
                if (axis == "R"):
                    return chiSep + chiral_identifiers["L"][i]
                if (axis == "L"):
                    return chiSep + chiral_identifiers["R"][i]
                if (axis == "F"):
                    return chiSep + chiral_identifiers["B"][i]
                if (axis == "B"):
                    return chiSep + chiral_identifiers["F"][i]
                if (axis == "U"):
                    return chiSep + chiral_identifiers["D"][i]
                if (axis == "D"):
                    return chiSep + chiral_identifiers["U"][i]
                #ugly, but good enough for now
    # old code, doesn't work anymore, but I might add chi-opposite pairs
    # either by constructing them here in f_name
    # or by storing them in a JSON
    # for pair in chiOpposite:
    #     if (pair[0] == chi):
    #         opp = pair[1]
    #     if (pair[1] == chi):
    #         opp = pair[0]
    # return opp

def ChiAxis(s):
    chiral_identifiers = GetDictJSON(fileProp="JSONchiral")
    s = s.split(GetSeperator("CHIRAL_IDENTIFIER"))[1] #annoyingly, I might be sending in chiSep
    if   ((s in chiral_identifiers["R"]) or (s in chiral_identifiers["L"])):
        return 'X'
    elif ((s in chiral_identifiers["F"]) or (s in chiral_identifiers["B"])):
        return 'Y'
    elif ((s in chiral_identifiers["U"]) or (s in chiral_identifiers["D"])):
        return 'Z'
    else:
        return 'NONE'

######## NUMBERS ########
def DetectNumber(s):
    #TODO CURRENTLY A COPY OF DETECTCHIRALs
    #NEEDS a way to find multiple numbers and record each in order
    #removedNumber should be a list of different levels of number removal
    # no numbers, one number, two numbers... -1 should be the name with one less number
    #is that a good idea?
    numbers = []
    removedNumber = [] 
    spl = s.split('.')
    numberIndices = []
    for ind in range(len(spl)):
        #why not an enumerator?
        try:
            numbers.append(int(spl[ind]))
            numberIndices.append(ind)
            removedNumber.append ("###")
        except ValueError:
            removedNumber.append(spl[ind])
    # n = -6 if (len(s) >= 6) else 0 
    # for x in chi:
    #     if ( s.find(x, (n) ) != -1):
    #         chiStr = x 
    #         foundChi += 1
    #         spl = str.split(removedChi, x)
    #         removedChi = ""
    #         for ind in range(len(spl) - 2):
    #             removedChi += spl[ind]
                #this should add together all of the substrings from
                # the split exceot the last one
                # which I want to remove
    #it's still possible more than one can be found, if for example .L.R occurs
    if (not numbers):
        #numbers is empty, assign 0
        numbers.append(0)

    return (numbers, removedNumber) #TODO clean up!

def DifferenceBetweenNameNumber(a, b, ind = 0):
    try:
        out = DetectNumber(b)[0][ind] - DetectNumber(a)[0][ind]
    except:
        out = None
    return out

def ChangeNameNumber(s, ind, num):
    pass
    # will need to impelemnt this eventually,
    #  if only for feature completeness.

######## QUERY FUNCTIONS ########
def FindBoneByPrefix(obArm, prefix, context):
    """returns a sorted list of names of bones sharing prefix"""
    bones = []
    if (context.mode == 'EDIT_ARMATURE'):
        for b in obArm.data.edit_bones:
            if (b.name.startswith(prefix, 0, len(prefix)) == True):
                bones.append(b.name)
    else:
        for b in obArm.data.bones:
            if (b.name.startswith(prefix, 0, len(prefix)) == True):
                bones.append(b.name)
    bones.sort()
    return bones
            
def FindBoneContainingString(obArm, string, context):
    """returns a sorted list of names of bones sharing string"""
    bones = []
    if (context.mode == 'EDIT_ARMATURE'):
        for b in obArm.data.edit_bones:
            if (string in b.name):
                bones.append(b.name)
    else:
        for b in obArm.data.bones:
            if (string in b.name):
                bones.append(b.name)
    bones.sort()
    return bones

def NewNameFromBone(bName, basename, prefix = '____', newBasename = '', suffix = ''):
    nums = DetectNumber(bName)[0]; chi = DetectChiral(bName)
    prefix = DetectPrefix(basename) if (prefix == "____") else prefix
    seperator = GetSeperator('PREFIX')
    if (prefix is None):
        prefix = ""
        seperator = ""
    socketName = prefix + seperator + (StripPrefix(basename) if (newBasename == "") else newBasename) + suffix
    for num in nums:
        socketName += '.' + str(num).zfill(3)
    if (chi[1]):
        socketName+=chi[0]
    return socketName

######## MISC FUNCTIONS ########
def randomString(length=12):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i  in range (length))

# testing
# if __name__ == "__main__":
#     bAct = bpy.context.active_bone
#     print (DetectChiral(bAct.name))
#     print (DetectNumber(bAct.name))