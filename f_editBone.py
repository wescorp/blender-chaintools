#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for Edit-Mode only functions"""
import bpy
from mathutils import Vector

from . import f_curve
from . import f_bone
from . import f_gen

def BonesFromCurve(obArm, obCrv, quantity = 8,
                   connected = True, parentBranch = False, baseName = "Bone",
                   set_radius = 1, set_roll = 1, tolerance = 0.1, fReport = None):
    """Generates Bones along a given curve object, returns a list with the
         names of the newly created bones"""
    import time
    start = time.time()
    print ("starting BonesFromCurve...")
    for b in obArm.data.edit_bones: #deselect bones
        b.select      = False
        b.select_head = False
        b.select_tail = False
    offset = obArm.location
    dupeCrv = obCrv.copy()
    dupeCrv.data = obCrv.data.copy()
    print ("getting data...")
    datastart = time.time()
    factorsList = []

    if (quantity > 0):
        factors = [(j * 1/quantity) for j in range(quantity+1)]
        factorsList = [factors for i in range(len(dupeCrv.data.splines))]
    else:# (quantity == 0), use curve points
        for spl in dupeCrv.data.splines:
            if (spl.type == 'BEZIER'):
                points = [bp.co + dupeCrv.location for bp in spl.bezier_points]
            else:
                points = [cv.co.to_3d() + dupeCrv.location for cv in spl.points]
            factorsList.append(f_gen.uValuesInPoints(points))
    f_curve.EnsureCurveIsRibbon(dupeCrv)
    data = f_curve.DataFromRibbon(dupeCrv, factorsList, fReport=fReport)
    print ("data collected in ", time.time() - datastart)
    bpy.data.curves.remove(dupeCrv.data) #removes the object, too
    newBones = []
    chains = []
    bonestart = time.time()
    totBones = 0
    for (points, radii, rolls) in data:
        totBones+= len(points) -1
    print ("creating ", totBones, " bones...")
    for (points, radii, rolls) in data:
        head_loc = points[0] - offset
        bPrev = None
        bones = []
        for i in range(len(points) - 1):
            tail_loc = points[i + 1] - offset
            b = obArm.data.edit_bones.new(baseName + '.' +str(i).zfill(3)) # want .000 too
            b.use_connect = connected
            b.parent = bPrev
            b.head = head_loc
            b.tail = tail_loc
            head_loc = tail_loc
            b.select      = True
            b.select_head = True
            b.select_tail = True
            if (set_roll != 0):
                bRoll = b.roll
                b.align_roll(rolls[i])
                b.roll = f_gen.lerpVal(bRoll, b.roll, set_roll)
            if (set_radius != 0):
                b.head_radius = radii[i]
                b.tail_radius = radii[i+1]
                b.envelope_distance = 0.01
            bPrev = b
            bones.append(b)
        # bones = list(filter(None.__ne__, bones)) #remove None
        # bones = [b.name for b in bones] #just take the names
        chains.append(bones)
        newBones+=bones
    print ("bone creation finished in ", time.time() - bonestart)
    print ("setting parents...")
    parentstart = time.time()
    if (parentBranch):
        from . import f_hier
        f_hier.CreateParentsAtBranchPoints(newBones, obArm, checkBetweenJoints = True, tolerance = tolerance)
    print ("parenting finished in ", time.time() - parentstart)
    print ("operation finished in", time.time() - start)
    #TODO make the return value work with names 
    return newBones #OK, this is where I should set it back to just names

def RelaxBoneChains(bones, obArm, select_method, factor):
    dictCrv, dictChain = f_bone.CurveFromBones(
                   bones,
                   obArm,
                   select_method = select_method,
                   combine_branches = True,
                   name = "relax_curve",
                   spline_type = 'POLY', #HACK rather fix this in RelaxBoneChain
                   is_evenly_distributed = False,
                   ) #POLY leads to choppiness, BEZIER should be OK
                     #BEZIER leads to failure in circle curves
    for i, crv in dictCrv.items():
        crv.data.extrude = 0; crv.data.bevel_depth = 0
        for spl in crv.data.splines:
            spl.use_cyclic_u = False
        chain = dictChain[i]
        if (len(chain) < 3):
            continue
        RelaxBoneChain(chain, obArm, crv, factor)
    for key in dictCrv.keys():
        bpy.data.curves.remove(dictCrv[key].data)
    
def RelaxBoneChain(chain, obArm, crv, factor):
    #TODO TODO TODO: add a fix if curve is Bezier
    bonePoints = f_bone.PointsFromBoneChain(chain, obArm)
    factorsList = f_curve.FindNearestPointOnCurve(crv, [bonePoints])
    factors_get = factorsList[0]
    # if (crv.data.splines[0].use_cyclic_u):
    #     if (factors_get[-1] == 0):
    #         factors_get[-1] = 1
    #         #I think I always want this
    #         #... or I could just snap the end...
    #ONLY IF BONES SHARE LOCATION
    location = True
    if (chain[0].tail != chain[1].head):
        location = False
    if (location == True):
        quantity = len(chain)
    else:
        quantity = len(chain) -1
    even = [(i * 1/quantity) for i in range(quantity)]
    even.append(1)
    factors = []
    for e, f in zip(even, factors_get):
        diff = f - e
        factors.append(((1 - factor) * diff) + e)
    points = f_curve.PointsFromCurve(crv, [factors])[0]
    #ensure that the first and last point remain identical
    points[0], points[-1] = bonePoints[0], bonePoints[-1]
    f_bone.SnapChain(chain, obArm, points, factor)
    if (crv.data.splines[0].use_cyclic_u):
        chain[-1].tail = chain[0].head

def SmoothBoneChains( bones,
                      obArm,
                      select_method,
                      iterations,
                      relax,
                      preserveVol,
                      factor):
    dictPolyCrv, dictChain = f_bone.CurveFromBones(
        bones,
        obArm,
        select_method = select_method,
        combine_branches = True, 
        name = "smoothing_curve",
        spline_type = 'NURBS',
        is_evenly_distributed = False )
        #using a poly makes the factor measurement more precise
    for i, chain in dictChain.items():
        if (len(chain) < 3):
            continue
        crv = dictPolyCrv[i]
        SmoothBoneChain(chain, obArm, crv, iterations, relax, preserveVol, factor)
    for key in dictPolyCrv.keys():
        bpy.data.curves.remove(dictPolyCrv[key].data)

def SmoothBoneChain(chain, obArm, crv, iterations, relax, preserveVol, factor):
    bbCenterBefore = f_bone.BoundingBoxCenterBones(chain)
    volumeBefore = f_bone.BoundingBoxVolumeBones(chain)
    # polyCrv = dictPolyCurves[i]
    bonePoints = f_bone.PointsFromBoneChain(chain, obArm)
    factors = f_curve.FindNearestPointOnCurve(crv, [bonePoints])
    if (crv.data.splines[0].use_cyclic_u == True):
        factors[0][-1] = factors[0][0]
    ###
    #Here's the actual smoothing
    for i in range(iterations):
        f_curve.SmoothSpline(crv, 0, factor)
    ###
    points = f_curve.PointsFromCurve(crv, factors)[0]
    f_bone.SnapChain(chain, obArm, points, factor)
    if (relax > 0):
        crvRel = f_bone.CurveFromChain(chain, obArm, "relax_spline", 'POLY', False)
        RelaxBoneChain(chain, obArm, crvRel, (relax) * (factor)) 
        #maybe I should just do both? But then I can't finish early if factor = 0
        bpy.data.curves.remove(crvRel.data)

    ###   Fix scale   ###
    volumeAfter = f_bone.BoundingBoxVolumeBones(chain)
    try:
        scaleFactorVol = (volumeBefore/volumeAfter)**(1/3)

        
        bbCenterAfter = f_bone.BoundingBoxCenterBones(chain)
        adjustedPoints = f_bone.PointsFromBoneChain(chain, obArm)
        translate = (bbCenterBefore - bbCenterAfter)
        for p in adjustedPoints:
            p+=translate
            displacement = p - bbCenterBefore
            displacement*= (scaleFactorVol -1 )
            p+=displacement
        f_bone.SnapChain(chain, obArm, adjustedPoints, preserveVol)
        #########################
        #cube root because we're dealing with a volume!
    except ZeroDivisionError:
        scaleFactorVol = 1 # do nothing
        #Not sure why I get a divide by 0 error.

def MirrorChain():
    pass

def InvertChain(chain, obArm, ):
    if (len(chain) < 2):
        b = chain[0]
        head = b.tail.copy()
        tail = b.head.copy()
        b.head = head
        b.tail = tail
        return
    points = f_bone.PointsFromBoneChain(chain, obArm)
    #doesn't work well in mixed chains or chains with lots of disconnected pieces
    # for p in points:
    #     p.freeze()
    # points = dict.fromkeys([p for p in points])
    #should work
    points.reverse()
    f_bone.SnapChain(chain, obArm, points, 1)

def SnapBonesToRibbonBySpline(
                     bones,
                     chain,
                     obArm,
                     obCrv,
                     use_whole_chain = True,
                     splineIndex = 0,
                     snap_points = 0,
                     snap_roll = 0,
                     snap_radius = 0,
                     fReport = None,
                    ):
    dupeCrv = obCrv.copy()
    dupeCrv.data = obCrv.data.copy()
    mat = dupeCrv.matrix_world
    dupeCrv.location = (0,0,0)
    f_curve.EnsureCurveIsRibbon(dupeCrv, defaultRadius = bones[0].head_radius)
    bonesInd = {}
    for i, b in enumerate(chain):
        if b in bones:
            bonesInd[b.name] = i
            if (len(bones) >= 2):
                if (use_whole_chain == True):
                    uValues = f_bone.uValuesInChain(chain, obArm)
                else:
                    uValues = f_bone.uValuesInChain(bones, obArm)
            else:
                uValues = [0,1]
    factorsList = [ uValues for i in range(len(dupeCrv.data.splines))]
    data = f_curve.DataFromRibbon(dupeCrv, factorsList, fReport)[splineIndex]
    dupeCrv.data.extrude = 0
    pointsBetter = f_curve.PointsFromCurve(dupeCrv, factorsList)[splineIndex]
    #for whatever reason, this works better in repeated operation
    bpy.data.curves.remove(dupeCrv.data)
    points, width, normals = data
    points = pointsBetter
    for i, p in enumerate(points):
        points[i] = obArm.matrix_world.inverted() @ (mat @ p)
        # HACK-- this should be handled elsewhere TODO
        #inverted because the armature object is the edit bone's parent
        # we need to multiply (its new position) by its inverse parent matrix
        #I don't think I need to adjust the normals
    snap, radius, roll = [], [], []
    for b in bones:
        ind = bonesInd[b.name] - len(points) #to avoid an IndexError
        snap.append(points[ind])
        radius.append(width[ind])
        roll.append(normals[ind])
    if ( ind < len(points) ):
        snap.append(points[ind + 1])
        radius.append(width[ind + 1])
    
    if (len(bones) < 2):
        #HACK: should be handled by SnapChain but isn't
        b = bones[0]
        b.head = b.head.copy().lerp(points[0] , snap_points)
        b.tail = b.tail.copy().lerp(points[-1], snap_points)
        b.head_radius = f_gen.lerpVal(b.head_radius, radius[0], snap_radius)
        b.tail_radius = f_gen.lerpVal(b.tail_radius, radius[-1], snap_radius)
        bRoll = b.roll
        b.align_roll(roll[0])
        b.roll = f_gen.lerpVal(bRoll, b.roll, snap_roll)
        # if (fReport):
        #     fReport(type = {'ERROR_INVALID_INPUT'}, message = "Select two or more bones.")
        return None

    f_bone.SnapChain( bones,
                        obArm,
                        snap,
                        snap_points,
                        radius = radius,
                        radiusFac = snap_radius,
                        roll = roll,
                        rollFac = snap_roll, )


#there's a bit of code duplication here, needs some more consideration
# I can put it all in the more efficient function if I can find a way to map the chain to the spline index
# maybe I should have the chainPairs parameter take a dict of {splineIndex: (bones, chain)}


def SnapBonesToRibbon(
                     chainPairs, #a list of tuples of chain, snapping chains
                     obArm,
                     obCrv,
                     use_whole_chain = True,
                     snap_points = 0,
                     snap_roll = 0,
                     snap_radius = 0,
                     fReport = None,
                    ):
    dupeCrv = obCrv.copy()
    dupeCrv.data = obCrv.data.copy()
    mat = dupeCrv.matrix_world
    dupeCrv.location = (0,0,0)
    f_curve.EnsureCurveIsRibbon(dupeCrv, defaultRadius = chainPairs[0][0][0].head_radius) #this is stupid :P
    bonesInd = {}
    factorsList = []
    for bones, chain in chainPairs:
        for i, b in enumerate(chain):
            if b in bones:
                bonesInd[b.name] = i
                if (len(bones) >= 2):
                    if (use_whole_chain == True):
                        uValues = f_bone.uValuesInChain(chain, obArm)
                    else:
                        uValues = f_bone.uValuesInChain(bones, obArm)
                else:
                    uValues = [0,1]
            factorsList.append(uValues)
    data = f_curve.DataFromRibbon(dupeCrv, factorsList, fReport)
    dupeCrv.data.extrude = 0
    pointsBetter = f_curve.PointsFromCurve(dupeCrv, factorsList)
    #for whatever reason, this works better in repeated operation
    bpy.data.curves.remove(dupeCrv.data)
    for i, pList in enumerate(pointsBetter):
        for p in pList:
            p = obArm.matrix_world.inverted() @ (mat @ p)
        # HACK-- this should be handled elsewhere TODO
        #inverted because the armature object is the edit bone's parent
        # we need to multiply (its new position) by its inverse parent matrix
        #I don't think I need to adjust the normals
    for i, (bones, chain) in enumerate(chainPairs):
        points, width, normals = data[i]
        points = pointsBetter[i]
        snap, radius, roll = [], [], []
        for b in bones:
            ind = bonesInd[b.name] - len(points) #to avoid an IndexError
            snap.append(points[ind])
            radius.append(width[ind])
            roll.append(normals[ind])
        if ( ind < len(points) ):
            snap.append(points[ind + 1])
            radius.append(width[ind + 1])
        
        if (len(bones) < 2):
            #HACK: should be handled by SnapChain but isn't
            b = bones[0]
            b.head = b.head.copy().lerp(points[0] , snap_points)
            b.tail = b.tail.copy().lerp(points[-1], snap_points)
            b.head_radius = f_gen.lerpVal(b.head_radius, radius[0], snap_radius)
            b.tail_radius = f_gen.lerpVal(b.tail_radius, radius[-1], snap_radius)
            bRoll = b.roll
            b.align_roll(roll[0])
            b.roll = f_gen.lerpVal(bRoll, b.roll, snap_roll)
            # if (fReport):
            #     fReport(type = {'ERROR_INVALID_INPUT'}, message = "Select two or more bones.")
            return None

        f_bone.SnapChain(   bones,
                            obArm,
                            snap,
                            snap_points,
                            radius = radius,
                            radiusFac = snap_radius,
                            roll = roll,
                            rollFac = snap_roll, )
