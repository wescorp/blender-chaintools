#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for managing relationships between bones

Keywords used throughout the module:
constraint: check bones for similar constraints using ConstraintCompare
location: check for bones sharing head/tail location
name: check for bones sharing base name and symmetry identifier
parent: check for bones with any parenting relationship
disconnected: check for bones with disconnected parenting relationship only
connected: check for bones with connected parenting relationship only


b: the bone to use for checka
obArm: the Object ID that owns the Armature ID containing b
"""
import bpy
from . import f_name

#lambda
GetAddonPref = lambda prop : (getattr(bpy.context.preferences.addons[__package__].preferences, prop))

#function
def MaskCompare(mask_a, mask_b):
    """Compares each element in mask_a and mask_b for equality."""
    for a, b in zip(mask_a, mask_b):
        if (a != b):
            return False
    return True
    import bpy

def GetVisible(b, obArm):
    visible = False
    for l1, l2 in zip(b.layers, obArm.data.layers):
        if ((l1 == True) and (l2 == True)):
            visible = True; break # bone only need to be in one visible layer
    if ((visible == True) and (b.hide == True)):
        visible = False
    return visible

def ConstraintCompare( a,
                       b,
                       obArm,
                       nameDiff= 1,
                       checkName = False,
                       checkTarget = False,
                       checkSubTarget = True):
    """Compares the constraints attatched to bones a and b.

    Arguments:
    a: the first bone  (can be editbone or bone)
    b: the second bone (can be editbone or bone)
    obArm: the Object ID that owns the armature ID containing a and b
    Keyword Arguments:
    nameDiff: the difference between names to accept,
    checkName: checks the constraint name.
    checkTarget: checks the name of the target of the constraint.
    checkSubTarget: checks the subtarget of the constraint.
    Return:
    If the constraints satisfies checks, returns True,
    otherwise, returns False."""
    # add = False
    assert (a.id_data == b.id_data)
    try:
        pb_a = obArm.pose.bones[a.name]
        pb_b = obArm.pose.bones[b.name]
    except:
        print ("Couldn't find pose bones. Try switching into object mode and back into edit mode")
        #TODO make an error and alert user, will need to pass in operator for this
        # #UI
        #This should be a rare event, gonna leave it as is for now.
        return False
    aSig = ConstraintSignature(pb_a)
    bSig = ConstraintSignature(pb_b)
    if ( ( (len(aSig) == 0) or (len(bSig) == 0) ) or (len(aSig) != len(bSig))):
        return False
    useMe = True
    for aC, bC in zip(aSig, bSig):
        aNumbers, aNames = aC[0], aC[1]
        bNumbers, bNames = bC[0], bC[1]
        for aNumTup, aNameTup, bNumTup, bNameTup in zip( aNumbers, aNames, bNumbers, bNames):
            if (aNameTup ==  bNameTup):
                if (checkName):
                    #check all but the last number for equality
                    for index in range(len(bNumTup[0]) - 2):
                        if (bNumTup[0][index] != aNumTup[0][index]):
                            useMe = False
                    #now check the last number for the difference
                    if ((bNumTup[0][-1] - aNumTup[0][-1]) != nameDiff):
                        useMe = False
                if (checkTarget):
                    for index in range(len(bNumTup[0]) - 2):
                        if (bNumTup[1][index] != aNumTup[1][index]):
                            useMe = False
                    if ((bNumTup[1][-1] - aNumTup[1][-1]) != nameDiff):
                        useMe = False
                if (checkSubTarget):
                    for index in range(len(bNumTup[0]) - 2):
                        if (bNumTup[2][index] != aNumTup[2][index]):
                            useMe = False
                    if ((bNumTup[2][-1] - aNumTup[2][-1]) != nameDiff):
                        useMe = False
            else:
                useMe = False
    if (useMe == True):
        return True
    return False

def ConstraintSignature(pb):
    """Returns the constraint signature of posebone pb, usable in comparison"""
    cSig = []
    for c in pb.constraints:
        pbNumbers = []
        pbNames = []
        cNum, cName   = f_name.DetectNumber(c.name)
        try:
            tNum, tName   = f_name.DetectNumber(c.target.name)
        except:
            tNum, tName = [0], [0]
        try:
            stNum, stName = f_name.DetectNumber(c.subtarget)
        except:
            stNum, stName = [''], ['']
        pbNumbers.append((cNum, tNum, stNum))
        pbNames.append((cName, tName, stName))
        cSig.append( (pbNumbers, pbNames) )
    return cSig
    
def DetectBranchDown(b,
                     obArm, 
                     constraint = False, 
                     location = False, 
                     name = False,
                     disconnected = False,
                     connected = True):
    """Detects branches downstream in the bone chain (if bone has multiple 'children')

    Each branching method is searched individually.
    Returns:
    True: if ANY branches are detected
    False: if no branches are detected"""
    if (b is not None):
        branch = []
        if (name):
            return False
        if (constraint):
            return False
            #there's no meaningful way to determine branches in these cases.
        if (location):
            branch += FindNextBone( b,
                                    obArm,
                                    constraint=False,
                                    location=True,
                                    name=False,
                                    disconnected=False,
                                    connected=False, )
        if (disconnected):
            branch += FindNextBone( b,
                                    obArm,
                                    constraint=False,
                                    location=False,
                                    name=False,
                                    disconnected=True,
                                    connected=False, )
        if (connected):
            branch += FindNextBone( b,
                                    obArm,
                                    constraint=False,
                                    location=False,
                                    name=False,
                                    disconnected=False,
                                    connected=True, )
        if (len(branch) > 1):
            return True
    return False

def DetectBranchUp(b,
                   obArm, 
                   constraint = False, 
                   location = False, 
                   name = False,
                   disconnected = False,
                   connected = True):
    """Detects Branches upstream in the chain (if bone has 'siblings')
    
    This is a convenience function combining FindPreviousBone 
    and DetectBranchDown."""
    if (b is not None):
        bCheck = FindPreviousBone( b,
                                   obArm,
                                   constraint = constraint,
                                   location = location,
                                   name = name,
                                   disconnected = disconnected,
                                   connected = connected, )
    if (not bCheck):
        return False
    return DetectBranchDown( bCheck[0], #FindPreviousBone should only ever return one
                             obArm,
                             constraint = constraint,
                             location = location,
                             name = name,
                             disconnected = disconnected,
                             connected = connected, )


def FindPreviousBone(b,
                     obArm, 
                     constraint = False, 
                     location = False, 
                     name = False, 
                     disconnected = False,
                     connected = True):
    """ Finds the previous bone in the chain
    
    This function only finds bones that satisfy ALL conditions.
    Returns: a list containing the previous bone in the chain, or empty list
    This function should only ever return with a length of 0 or 1.
    It uses a list to pass data for consistency with FindNextBone"""
    prevBones = []
    # counter = 0
    if ( (constraint == False) and (location == False) and
        (name == False) and (disconnected == False)    and
        (connected == False)                               ):
        return prevBones
    if ((b.parent)):
        #checking the preferences everytime is a bit overkill, but OK for now
        if ( ( (GetAddonPref(prop="checkLayers") ) and 
               (MaskCompare(b.layers, b.parent.layers) == True)) or
             (GetAddonPref(prop="checkLayers") == False) ):
            if (obArm.mode == 'EDIT'):
                headLoc = b.head
                tailLoc = b.parent.tail
            else:
                headLoc = b.head_local
                tailLoc = b.parent.tail_local
            # it's OK to return early from this function
            #  since there can only be one previous bone
            #  in the case of parenting.
            if ((b.use_connect == True ) and (connected == True)):
                return [b.parent] 
            if ((headLoc == tailLoc) and (b.use_connect == False) and
                (disconnected == True)):
                return [b.parent]
    if ((connected == False) and (disconnected == False)):
        if (obArm.mode == 'EDIT'):
            boneList = obArm.data.edit_bones
        else:
            boneList = obArm.data.bones
        for bOther in boneList:
            if (GetVisible(bOther, obArm) == False):
                continue
            if (bOther.name == b.name):
                continue
            if ( (GetAddonPref(prop="checkLayers") ) and (MaskCompare(b.layers, bOther.layers) != True) ):
                continue
            else:
                if (obArm.mode == 'EDIT'):
                    headLoc = b.head
                    tailLoc = bOther.tail
                else:
                    headLoc = b.head_local
                    tailLoc = bOther.tail_local
                if (headLoc != tailLoc):
                    if ((location == True)):
                        continue
                if (name == True):
                    if (b.basename != bOther.basename):
                        continue
                    else:
                        numDiff = f_name.DifferenceBetweenNameNumber(b.name, bOther.name, -1)
                        chiDiff = f_name.compareChiral(b.name, bOther.name)
                        if (chiDiff != True) or (numDiff != -1):
                            continue
                if (constraint):
                    if (ConstraintCompare(b, bOther, obArm, -1) != True):
                        continue
            prevBones.append(bOther)
            # Still prioritizes parents- I think this is the best way to do this.
        if (len(prevBones) > 1):
            print ("Found more than one possible previous bone.")
            #TODO alert user in a nicer way UI
            #and maybe return [] instead
    return prevBones

def FindNextBone(b,
                 obArm, 
                 constraint = False, 
                 location = False, 
                 name = False,
                 disconnected = False,
                 connected = True):
    """ Finds the next bone in the chain

    This function only finds bones that satisfy ALL conditions.
    returns: a list of next bones, or []"""

    nextBones = []
    if (b is not None):
        if (obArm.mode == 'EDIT'):
            boneList = obArm.data.edit_bones
        else:
            boneList = obArm.data.bones
        for bOther in boneList:
            if (GetVisible(bOther, obArm) == False):
                continue
            if ( (GetAddonPref(prop="checkLayers") ) and (MaskCompare(b.layers, bOther.layers) != True) ):
                continue
            else:
                if (obArm.mode == 'EDIT'):
                    headLoc = bOther.head
                    tailLoc = b.tail
                else:
                    headLoc = bOther.head_local
                    tailLoc = b.tail_local
                if (connected and disconnected):
                    if ((tailLoc != headLoc) or
                        (bOther.parent != b)):
                        continue
                if (connected and not disconnected):
                    if ((bOther.parent != b) or (bOther.use_connect != True)):
                        continue
                if (disconnected and not connected):
                    if ((tailLoc != headLoc) or
                        (bOther.parent != b)    or
                        (bOther.use_connect == True)):
                        continue
                if (location):
                    if (tailLoc != headLoc):
                        continue
                if (name):
                    if (b.basename != bOther.basename):
                        continue
                    numDiff = f_name.DifferenceBetweenNameNumber(b.name, bOther.name, -1)
                    chiDiff = f_name.compareChiral(b.name, bOther.name)
                    if (chiDiff != True) or (numDiff != 1):
                        continue
                if (constraint):
                    if (ConstraintCompare(b, bOther, obArm, 1) != True):
                        continue
            nextBones.append(bOther)
    return nextBones


# def FindFirstBone(b,
#                   obArm,
#                   constraint = False,
#                   location = False,
#                   name = False,
#                   disconnected = False,
#                   connected = True):
#     #is this needed? But it might be useful.
#     #TODO
#     return None

def GetBoneChain(b,
                 obArm,
                 parent_extend = False,
                 constraint = False, 
                 location = False, 
                 name = False,
                 disconnected = False,
                 connected = True,):
    """Get the chain the b belongs to

    parent_extend: whether to select the chain that current chain branches off of  
    returns: a tuple with ( [bones], [branch heads])"""
    chain     = []
    branches  = []
    prevBones = []
    nextBones = []
    bPrev = FindPreviousBone(b,
                             obArm,
                             constraint,
                             location,
                             name,
                             disconnected,
                             connected)
    bNext  = FindNextBone(b,
                          obArm,
                          constraint,
                          location,
                          name,
                          disconnected,
                          connected)
    while ( bPrev ):
        if ((DetectBranchDown(bPrev[0], 
                              obArm,
                              constraint,
                              location,
                              name,
                              disconnected,
                              connected) == True) and (parent_extend == False)):
            break
        for bP in bPrev:
            prevBones.append(bP)
        addPrev = []
        try:
            for bCheck in bPrev:
                addPrev = [bAdd for bAdd in FindPreviousBone(bCheck,
                                            obArm,
                                            constraint, 
                                            location, 
                                            name,
                                            disconnected,
                                            connected)]
            bPrev = addPrev
        except:
            bPrev = []
    while ( (len(bNext) == 1) ):
        for bN in bNext:
            nextBones.append(bN)
        addNext = []
        try:
            for bCheck in bNext:
                addNext += [bAdd for bAdd in FindNextBone(bCheck,
                                                obArm,
                                                constraint, 
                                                location, 
                                                name,
                                                disconnected,
                                                connected)]
            bNext = addNext
        except:
            bNext = []
    if (len(bNext) > 1):
        for branch in bNext:
            branches.append(branch)
    prevBones.reverse()
    chain += prevBones + [b] + nextBones
    return (chain, branches)

def GetBoneChainName(b, obArm):
    """Optimized function for getting chain b belongs to by name"""
    #BUG- this doesn't not scale well in large rigs
    bones = []
    if (obArm.mode == 'EDIT'):
        boneList = obArm.data.edit_bones
    else:
        boneList = obArm.data.bones
    for bOther in boneList:
        chiDiff = f_name.compareChiral(b.name, bOther.name)
        if ((b.basename == bOther.basename) and (chiDiff == True)) :
            bones.append(bOther)
    bones.sort(key=lambda x: x.name)
    #check for gaps
    prevName = bones[0].name #this ensures no difference on first go-around
    curName  = bones[0].name
    sortedBones = {}
    segment = 0
    for bone in bones:
        curName = bone.name
        if ( f_name.DifferenceBetweenNameNumber(prevName, curName, -1) == 1):
            sortedBones[segment].append(bone)
        else:
            segment+=1
            sortedBones[segment] = [bone]
        prevName = curName
    for seg in sortedBones.values():
        if b in seg:
            return (seg, []) #empty list for consistency with GetBoneChain
    return ([],[])           #but of course name numbers can't branch


def GetBoneChainConnected(b, branch_extend = False):
    #Temporary    HACK HACK HACK HACK HACK
    return (GetBoneChain(b,
                 bpy.context.active_object,
                 branch_extend,
                 constraint=False,
                 location=False,
                 name = False,
                 disconnected = False,
                 connected = True, ) )
                 #end HACK


def ChainFromBones( bones,
                    obArm,
                    combine_branches = True,
                    constraint = False,
                    location = False,
                    name = False,
                    disconnected = False,
                    connected = False, ):
    """ Finds bone chains within bones
    
    Keyword Arguments:
    combine_branches: Whether to combine chains if they belong to a branch structure
                        note that this will only combine the first branched chain
    """
    chainCheck = bones.copy()
    for bone in bones:
        prev = FindPreviousBone( bone,
                                 obArm,
                                 constraint,
                                 location,
                                 name,
                                 disconnected,
                                 connected)
        #under normal circumstances, prev will be 1 or 0 bones.
        if (prev):
            if (prev[0] in bones):
                chainCheck.remove(bone)
            if (DetectBranchDown( prev[0],
                                obArm,
                                constraint,
                                location,
                                name,
                                disconnected,
                                connected) == True):
                chainCheck.append(bone)# we know it has 'siblings'
                #THIS IS HAPPENING BECAUSE NAME/LOCATION DETECTS BRANCHES DIFFERENTLY
                # I don't know what this comment means?
    chains = []
    for bCheck_a in chainCheck:
        chain = GetBoneChain( bCheck_a,
                            obArm,
                            False,
                            constraint,
                            location,
                            name,
                            disconnected,
                            connected)[0]
        add = []
        for bCheck_b in chain:
            if (bCheck_b in bones):
                add.append(bCheck_b)
        if (add not in chains): #avoid duplicates 
            #TODO find a way to avoid creating dupes in the first place
            chains.append(add)
        #This  part is why it doesn't treat gaps as seperate chains
        #I donno, this is good for making curves but less consistent?
    if (combine_branches == True):
        check = True
        branchPoints = []
        while (check == True):
            check = False
            breakMe = False
            dupeChains = chains.copy()
            # Get Branch Heads
            for chain in chains:
                for bCheck_c in chain:
                    nb = FindNextBone( bCheck_c,
                                    obArm,
                                    constraint,
                                    location,
                                    name,
                                    disconnected,
                                    connected)
                    if (len(nb) > 1):
                        for i in range(len(nb)):
                            branchPoints.append((bCheck_c, nb[i]))
            # Combine Branches
            for chain_a in dupeChains:
                for chain_b in dupeChains:
                    if (chain_b is not chain_a):
                        for branch in branchPoints:
                            if ( (chain_a[-1] == branch[0]) and (chain_b[0] == branch[1]) ):
                                chain_extended = chain_a + chain_b
                                dupeChains.remove(chain_a)
                                dupeChains.remove(chain_b)
                                dupeChains.append(chain_extended)
                                breakMe = True
                                check = True
                                break
                    if (breakMe):
                        break
                if (breakMe):
                    break
            # ugly, but it works
            chains = dupeChains.copy()
    return chains

def CreateParentsAtBranchPoints(bones, obArm, checkBetweenJoints, tolerance, disconnected = True):
    from . import f_bone
    bonePairs = f_bone.FindBranchPoints(bones, obArm, checkBetweenJoints, tolerance)
    for pair in bonePairs:
        if (disconnected == True):
            pair[0].use_connect = False
        pair[0].parent = pair[1]

################################################################
# Updates
################################################################


def UpdateChildren(bones):
    for b in bones:
        for ch in b.children:
            if (ch.use_connect == True):
                ch.head = b.tail

# def UpdateMirrorBones(bones, obArm):
#     #I haven't seen a problem with bone heads
#     # or any tail in connected bones except the last
#     for b in bones:
#         print (b.name)
#         if (obArm.mode == 'EDIT'):
#             opp = FindSymmetryBone(b, obArm)[0]
#             print (opp.name)
#             if (opp):
#                 print (b.tail)
#                 t = b.tail.copy()
#                 oppTail = Vector ((t[0] * -1, t[1], t[2]))
#                 opp.tail = oppTail
#                 print (b.tail)
#doesn't work. Bug? Will handle in SnapChain() instead

#########################################
# Some additional useful functions
#########################################
def FindSymmetryBone(b, obArm):
    bone = None
    oppChi = None
    chi, detected, base = f_name.DetectChiral(b.name)
    if (detected == True):
        oppChi = f_name.GetChiOpposite(chi)
        search = base + oppChi
        try:
            bone = obArm.data.edit_bones[search]
        except AttributeError: #wrong mode
            bone = obArm.data.bones[search]
        except KeyError: # bone doesn't exist
            bone = None
    return bone, oppChi


def CreateIntermediateParent():
    pass

def ConvertParentRelationship():
    pass
    #Might not actually bother to implement this; it's not clear if it will be useful