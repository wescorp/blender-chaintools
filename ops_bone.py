#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for mode-agnostic operators"""
import bpy
from . import ops_base
from . import f_bone
from . import f_gen

class RenameBoneChain(ops_base.ChainToolsOperator):
    #Should this operate on the active bone, as it does?
    #Maybe Extend Branch could use an enum property to give the rigger
    # a list of bones to extend the selection to?
    #
    #Should have an enum property for Selection Method:
        #Connected
        #Disconnected, by parents
        #Disconnected, by location
        #By Name
    """Rename Bone Chain"""
    bl_idname = "object.rename_bone_chain"
    bl_label = "Rename Bone Chain"
    bl_options = {'REGISTER', 'UNDO'}
    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and ((
                 context.mode == 'EDIT_ARMATURE') or (context.mode == 'POSE')))

    select_method: bpy.props.EnumProperty(
                            items = ops_base.ChainToolsOperator.select_method_enum_flag,
                            name = "Selection Method",
                            options = {'ENUM_FLAG'},
                            default = f_gen.GetAddonPref("defaultSelectionMethod"),)
    name: bpy.props.StringProperty(
                            name ="Chain Name",
                            description = "New basename for bone chain",
                            default = "Chain")
    symmetry: bpy.props.EnumProperty(
                            items = [
                                ('X', "X-Axis", "Symmetry along X-Axis", 0),
                                ('Y', "Y-Axis", "Symmetry along Y-Axis", 1),
                                ('Z', "Z-Axis", "Symmetry along Z-Axis", 2),
                                ('NONE', "None", "Do not name symmetrically", 3),
                                    ],
                            name = "Use Symmetry",
                            description = " Add chiral identifier to end of name and rename symmetrical bones, if they exist",
                            default = 'X',
                                    )
    renameOpp: bpy.props.BoolProperty(
                            name = "Rename Opposite Bones",
                            description = "If symmetry is enabled, rename bones sharing symmetrical names. This will not rename bones unless they have the opposite chiral identifier to the current chain.",
                            default = True)
    increment: bpy.props.IntProperty(
                            name = "Number By",
                            description = 'Number by which to increment names. Positive integers increment, negative integers decrement, and 0 will leave existing numbers in place. Decrementing will not allow negative numbers in bone names, instead adjusting the offset automatically',
                            default = 1)
    offset: bpy.props.IntProperty(
                            name = "Offset Bone Numbers",
                            description = "Number by which to offset names, in addition to the increment amount.",
                            default = 0,
                            min = 0)
    child_extend: bpy.props.BoolProperty(
                            name = "Extend Child Branch")
    parent_extend: bpy.props.BoolProperty(
                            name = "Extend Parent Branch")

    def execute(self, context):
        obArm = context.active_object
        if (context.mode == 'EDIT_ARMATURE'):
            bone = context.active_bone
            for b in context.selected_editable_bones: 
                b.select      = False
                b.select_head = False
                b.select_tail = False
        else:
            bone = context.active_pose_bone.bone
            for b in context.selected_pose_bones: 
                b.bone.select      = False
                b.bone.select_head = False
                b.bone.select_tail = False

        f_bone.RenameBoneChain( bone,
                                obArm,
                                self.name,
                                self.symmetry,
                                self.renameOpp,
                                self.increment,
                                self.offset,
                                self.parent_extend,
                                self.child_extend,
                                self.select_method)
                                # probably should just pass self            
        return {"FINISHED"}

class SelectBoneChain(ops_base.ChainToolsOperator):
    """Select Bone Chain"""
    bl_idname = "object.select_bone_chain"
    bl_label = "Select Bone Chain"
    bl_options = {'REGISTER', 'UNDO'}
    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and ((
                 context.mode == 'EDIT_ARMATURE') )  or (context.mode == 'POSE'))
    # I initially made this operator to test the functions I'm developping
    # But I think I'm going to include it in the addon. It's handy.
    # name: bpy.props.StringProperty(
    #                         name ="Chain Name",
    #                         description = "New basename for bone chain",
    #                         default = "")
    # offset: bpy.props.IntProperty(
    #                         name = "Number of First Bone",
    #                         default = 0,
    #                         min = 0)
    select_method: bpy.props.EnumProperty(
                            items = ops_base.ChainToolsOperator.select_method_enum_flag,
                            name = "Selection Method",
                            options = {'ENUM_FLAG'},
                            default = f_gen.GetAddonPref("defaultSelectionMethod"),)
    child_extend: bpy.props.BoolProperty(
                            name = "Extend Child Branch")
    parent_extend: bpy.props.BoolProperty(
                            name = "Extend Parent Branch")

    def execute(self, context):
        if (context.mode == 'EDIT_ARMATURE'):
            bone = context.active_bone
        else:
            bone = context.active_pose_bone.bone
        obArm = context.active_object
        chain = f_bone.SelectChain( bone,
                                      obArm,
                                      self.select_method,
                                      self.parent_extend,
                                      self.child_extend)
        for b in chain:
            if (b is not None):
                b.select      = True
                b.select_head = True
                b.select_tail = True
        return {"FINISHED"}

class CreateCurveFromBones(ops_base.ChainToolsOperator):
    """Creates a curve passing through bones"""
    bl_idname = "object.create_curve_from_bones"
    bl_label = "Create Curve From Selected Bones"
    bl_options = {'REGISTER', 'UNDO'}
    
    #ToDo:
        #Add option to automatically create SplineIK
        #maybe: pay more attention to placement of handles
    
    select_method: bpy.props.EnumProperty(
                            items = ops_base.ChainToolsOperator.select_method_enum_flag,
                            name = "Selection Method",
                            options = {'ENUM_FLAG'},
                            default = f_gen.GetAddonPref("defaultSelectionMethod"),)
    spline_type: bpy.props.EnumProperty(
                            items = [('BEZIER',"Bezier", "Bezier", 0), 
                             ('NURBS',"NURBS", "NURBS", 1), 
                             ('POLY',"Poly", "Poly", 2)],
                             #('CARDINAL',"Cardinal", "Cardinal", 3), #documented but
                             #('BSPLINE',"B-Spline", "B-Spline", 4), ], #unimplemented?
                            name = "Type of Curve",
                            default = 'BEZIER')
    name: bpy.props.StringProperty(
                            name ="Curve Name",
                            description = "Name for new curve object",
                            default = "")
    is_evenly_distributed: bpy.props.BoolProperty(
                            name = "Evenly Distributed Control Points")
    extend: bpy.props.BoolProperty(
                            name = "Extend Branches")


    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and ((
                 context.mode == 'EDIT_ARMATURE') or (context.mode == 'POSE')))
                 
    def execute (self, context):
        obArm = context.active_object
        if (context.mode == 'EDIT_ARMATURE'):
            bones = context.selected_editable_bones
        else:
            bones = [pb.bone for pb in context.selected_pose_bones]
        
        dictCrv = f_bone.CurveFromBones( bones,
                                           obArm,
                                           self.select_method,
                                           self.extend,
                                           self.name,
                                           self.spline_type,
                                           self.is_evenly_distributed,
                                           )[0]
        if (len(dictCrv) > 0):
            for crv in dictCrv.values():
                context.collection.objects.link(crv)
        self.report(type={"INFO"}, message="Created "+ str(len(dictCrv)) + " curve objects")
        return {'FINISHED'}


class SetChainEnvelopeFromCurve(ops_base.ChainToolsOperator):
    """Creates a curve passing through bones"""
    bl_idname = "object.set_chain_envelope_from_curve"
    bl_label = "Set Chain Envelope From Curve"
    bl_options = {'REGISTER', 'UNDO'}

    def populate_curves_list(self, context):
        #eventually add curve icon
        retList = []
        curves = []
        i = 0
        for ob in context.selected_objects:
            #this way, selected objects are higher on the list
            if (ob.type == 'CURVE'):
                if (len (ob.data.splines) > 0):
                    data_add = (ob.name, ob.name, "Curve to Create Bones Along", i)
                    retList.append(data_add)
                    curves.append(ob.name)
                    i+=1
        for ob in bpy.data.objects:
            if ((ob.type == 'CURVE') and (ob.name not in curves)):
                if (len (ob.data.splines) > 0):
                    data_add = (ob.name, ob.name, "Curve to Create Bones Along", i)
                    retList.append(data_add)
                    i+=1
        if (i == 0):
            retList = [(None, None, None, 0)]
        return(retList)
        #COPIED FROM ABOVE, DE-DUPLICATE TODO TODO HACK HACK
        # I think I can do this by defining a subclass and inheriting.
    
    select_method: bpy.props.EnumProperty(
                            items = ops_base.ChainToolsOperator.select_method_enum_flag,
                            name = "Selection Method",
                            options = {'ENUM_FLAG'},
                            default = f_gen.GetAddonPref("defaultSelectionMethod"),)
    target: bpy.props.EnumProperty(
                items = populate_curves_list,
                name = "Target Curve" )
    splineIndex: bpy.props.IntProperty(
                name="Spline Index",
                description="Spline to be used for snapping bones to",
                default = 0,
                subtype='UNSIGNED')
    use_whole_chain: bpy.props.BoolProperty(
                name="Use Whole Chain",
                default = False,
                description = "Use whole chain when calculating u-value along curve used for snapping joints",
                ) #TODO add description, use better name

    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and ((
                 context.mode == 'EDIT_ARMATURE') or (context.mode == 'POSE')))
                 
    def execute (self, context):
        obArm = context.active_object
        try:
            obCrv = bpy.data.objects[self.target]
        except:
            obCrv = None
        pose_bones = context.selected_pose_bones_from_active_object
        bones = []
        for pb in pose_bones:
            bones.append(pb.bone)
        bone = context.active_bone
        if (len(bones) < 2):
            self.report({'ERROR_INVALID_INPUT'}, "Select two or more bones")
            return {'CANCELLED'}
        if (self.splineIndex > (len(obCrv.data.splines) - 1) ):
            self.splineIndex = f_gen.cap(self.splineIndex, (len(obCrv.data.splines) - 1) )
            self.report(type = {"OPERATOR"}, message = "Spline Index out of range, using last spline.")
        elif (self.splineIndex < ((-1 * len(obCrv.data.splines) + 1)) ):
            self.splineIndex = 0
            self.report(type = {"OPERATOR"}, message = "Spline Index out of range, using first spline.")
        if (obCrv is not None):
            chains = f_bone.ChainsInSelection(
                bones,
                obArm,
                self.select_method,
                False,
            )
            for bones in chains:
                if (bone in bones):
                    chain = f_bone.SelectChain(
                        bones[0],
                        obArm,
                        self.select_method,
                        False,
                        False,
                        )
                    f_bone.SetBoneDataFromRibbon(
                        bones, 
                        chain, 
                        obArm, 
                        obCrv,
                        self.use_whole_chain,
                        self.splineIndex,
                        fReport = self.report, )
        return {"FINISHED"}
