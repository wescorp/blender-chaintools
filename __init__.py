#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
bl_info = {
    "name": "ChainTools",
    "author": "Joseph Brandenburg",
    "version": (0, 0, 93.2),
    "blender": (2, 80, 0),
    "location": "Armature Edit Mode Menu",
    "description": "Assists rigger in creating and managing bone chains",
    "warning": "",
    "wiki_url": "https://gitlab.com/josephbburg/blender-chaintools/wikis/Home",
    "tracker_url": "https://gitlab.com/josephbburg/blender-chaintools",
    "category": "Rigging"}
import bpy

###############################################################
# MENUS
###############################################################
class ChainToolsEditModeMenu(bpy.types.Menu):
    bl_label = 'ChainTools'
    bl_idname = 'ARMATURE_MT_ChainTools'
    #TODO: add poll function for this
    def draw(self, context):
        self.layout.operator('object.rename_bone_chain')
        self.layout.operator('object.select_bone_chain')
        self.layout.operator('object.create_curve_from_bones')
        self.layout.operator('armature.create_bones_along_curve')
        self.layout.operator('armature.smooth_bone_chain')
        self.layout.operator('armature.relax_bone_chain')
        self.layout.operator('armature.snap_bones_to_curve')
        self.layout.operator('armature.invert_chain')
        self.layout.operator('armature.create_parents_at_chain_branch')
        self.layout.operator('armature.create_socket_bones')
        self.layout.operator('armature.parent_chain_to_chain')

# class ChainToolsPoseModeMenu(bpy.types.Menu):
#     bl_label = 'ChainTools'
#     bl_idname = 'OBJECT_MT_ChainTools'

#     def draw(self, context):
#         layout = self.layout
#         self.layout.operator('object.rename_bone_chain')
#         self.layout.operator('object.select_bone_chain')
#
#
#VIEW3D_MT_pose_context_menu

def menu_func(self, context):
    self.layout.menu('ARMATURE_MT_ChainTools')

def menu_func_add(self,context):
    self.layout.operator('object.create_bones_along_curve', icon='OUTLINER_OB_ARMATURE', text="Create Armature From Selected Curve")

###############################################################
# REGISTRATION
###############################################################
def ClassesInModule(module):
    #from https://stackoverflow.com/questions/5520580/how-do-you-get-all-classes-defined-in-a-module-but-not-imported
    #Thanks, piRSquared
    md = module.__dict__
    return [
        md[c] for c in md if (
            isinstance(md[c], type) and md[c].__module__ == module.__name__ )]

def FillClassList():
    import inspect #for listing classes
    from . import prefs
    from . import ops_bone
    from . import ops_editBone
    from . import ops_poseBone
    from . import ops_misc
    modules = [ops_bone, ops_editBone, ops_poseBone, ops_misc, prefs]
    classes = []
    for module in modules:
        classes += ClassesInModule(module)
    return classes

classes = FillClassList() + [ChainToolsEditModeMenu]

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.VIEW3D_MT_armature_context_menu.append(menu_func)
    bpy.types.VIEW3D_MT_pose_context_menu.append(menu_func)
    bpy.types.VIEW3D_MT_armature_add.append(menu_func_add)
def unregister():
    for cls in reversed(classes):
        try:
            bpy.utils.unregister_class(cls)
        except RuntimeError:
            #the class was never registered
            # I think this happens because I have a few classes
            # that aren't operators
            pass
    bpy.types.VIEW3D_MT_armature_context_menu.remove(menu_func)
    bpy.types.VIEW3D_MT_pose_context_menu.remove(menu_func)
    bpy.types.VIEW3D_MT_armature_add.remove(menu_func_add)
if (__name__ == "__main__"):
    register()
