#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for mesh geometry functions""" 
import bpy
import mathutils
import bmesh
from mathutils import Vector
from math import sqrt

from . import f_gen

################################################################################
#Wire Mesh
################################################################################

# def StripSpline(datCrv, splIndex):
#     #works, but isn't currently being used
#     #also kinda ugly
#     datNew = datCrv.copy()
#     for i, spl in enumerate(datNew.splines):
#         if (i != splIndex):
#             datNew.splines.remove(spl)
#     assert (len (datNew.splines) == 1), "Function has failed: " + str(len(datNew.splines))
#     return datNew
    
def DetectWireEdges(mesh):
    # Returns a list of vertex indices belonging to wire meshes
    # NOTE: this assumes a mesh object with only wire meshes
    # ---DO NOT call this script with a mesh that has polygons!--- #
    bm = bmesh.new()
    bm.from_mesh(mesh)
    ends = []
    for v in bm.verts:
        if (len(v.link_edges) == 1):
            ends.append(v.index)
    for e in bm.edges:
        assert (e.is_wire == True),"This function can only run on wire meshes"
        if (e.verts[1].index - e.verts[0].index != 1):
            ends.append(e.verts[1].index)
            ends.append(e.verts[0].index)
    ret = []
    for i in range(len(ends)//2): # // is floor division
        beg = ends[i*2]
        end = ends[(i*2)+1]
        indices = [(j + beg) for j in range ((end - beg) + 1)]
        ret.append(indices)
    return ret
    
def WireMeshEdgeLengths(m, wire):
    circle = False
    vIndex = wire.copy()
    for e in m.edges:
        if ((e.vertices[0] == vIndex[-1]) and (e.vertices[1] == vIndex[0])):
            #this checks for an edge between the first and last vertex in the wire
            circle = True
            break
    #can't I just check if every vertex has two edges connected?
    # bm = bmesh.new()
    # bm.from_mesh(m)
    # ends = []
    # for v in bm.verts:
    #     if (len(v.link_edges) != 2):
    #         ends.append(v.index)
    #
    #this works too, not sure I like it better, though
    #
    lengths = []
    for i in range(len(vIndex)):
        v = m.vertices[vIndex[i]]
        if (circle == True):
            vNextInd = vIndex[f_gen.wrap((i+1), len(vIndex) - 1)]
        else:
            vNextInd = vIndex[f_gen.cap((i+1), len(vIndex) - 1 )]
        vNext = m.vertices[vNextInd]
        lengths.append(( v.co - vNext.co ).length)
    #if this is a circular wire mesh, this should wrap instead of cap
    return lengths

def CalcLengthWireMesh(m, wire):
    return sum(WireMeshEdgeLengths(m, wire))

def GetDataFromWire(m, wire):
    #maybe this could be incorporated into the DetectWireEdges function?
    #maybe I can check for closed poly curves here? under what other circumstance
    # will I find the ends of the wire have identical coordinates?
    vertData = []
    vIndex = wire.copy()
    #
    #
    lengths = WireMeshEdgeLengths(m, wire)
    lengths.append(0)
    totalLength = sum(lengths)
    for i, vInd in enumerate(vIndex):
        # vNext = f_gen.wrap((vInd +1), (len(vIndex) + vIndex[0]) - 1)
        #-1 to avoid IndexError
        vNext = vIndex[ (f_gen.wrap(i+1, len(vIndex) - 1)) ]
        vertData.append((vInd, vNext, lengths[i]))
    #if this is a circle, the last v in vertData has a length, otherwise 0
    return vertData, totalLength

def PointsFromWireMesh(m, factorsList, obLoc):
    #Note, factors list should be equal in length the the number of wires
    #Now working for multiple wires, ugly tho
    wires = DetectWireEdges(m)
    ret = []
    for factors, wire in zip(factorsList, wires):
        points = []
        vertData, totalLength = GetDataFromWire(m, wire)
        for fac in factors:
            targetLength = totalLength * fac
            curVert = vertData[0]
            curLength = 0
            for j in (vertData):
                if (curLength >= targetLength):
                    break
                curLength += j[2]
                curVert = j
            targetLengthAtEdge = (curLength - targetLength)
            if (targetLength == 0):
                curFac = 0
            elif (targetLength == totalLength):
                curFac = 1
            else:
                try:
                    curFac = 1 - (targetLengthAtEdge/ curVert[2])
                except ZeroDivisionError:
                    curFac = 0
                    print ("DIVISION BY ZERO!!!")
            #Ugly, but OK for now
            loc1 = m.vertices[curVert[0]].co
            loc2 = m.vertices[curVert[1]].co
            if ((curFac > 0) or (curFac < 1)):
                outPoint = loc1.lerp(loc2, curFac)
            elif (curFac == 0):
                outPoint = loc1.copy()
            elif (curFac == 1):
                outPoint = loc2.copy()
            outPoint += obLoc
            points.append(outPoint.copy())
        ret.append(points)
    return ret # this is a list of lists

def FindNearestPointOnWireMesh(m, pointsList):
    wires = DetectWireEdges(m)
    ret = []
    # prevFactor = None
    for wire, points in zip(wires, pointsList):
        vertData, total_length = GetDataFromWire(m, wire)
        factorsOut = []
        for p in points:
            prevDist = float('inf')
            curDist  = float('inf')
            v1 = None
            v2 = None
            for i in range(len(vertData) - 1):
                #but it shouldn't check the last one
                if (p == m.vertices[i].co):
                    v1 = vertData[i]
                    v2 = vertData[i+1]
                    offset = 0
                    break
                else:
                    curDist = ( (sqrt((m.vertices[vertData[i][0]].co - p).length)) +
                                (sqrt((m.vertices[vertData[i][1]].co - p).length)) )/2
                if (curDist < prevDist):
                    v1 = vertData[i]
                    v2 = vertData[i+1]
                    prevDist = curDist
                    offset = mathutils.geometry.intersect_point_line(p, m.vertices[v1[0]].co, 
                                                                m.vertices[v2[0]].co)[1]
            if (offset < 0):
                offset = 0
            elif (offset > 1):
                offset = 1
            # Assume the vertices are in order
            v1Length = 0
            v2Length = v2[2]
            for i in range(v1[0]):
                v1Length += vertData[i][2]
            factor = ((offset * (v2Length)) + v1Length )/total_length
            # if ((prevFactor is not None) and (factor < prevFactor)):
            #     factor+=1
            factor = f_gen.wrap(factor, 1, 0) # doesn't hurt to wrap it if it's over 1 or less than 0
            factorsOut.append(factor)
            # prevFactor = factor
        if (vertData[-1][2] == 0):
            #this means we aren't dealing with a closed curve
            factorsOut[0] = 0
            factorsOut[-1] = 1 #just keep the ends in place
        ret.append( factorsOut )
    return ret

def SmoothWireMesh(m):
    #This function assumes that m is not associated with any object
    # it also assumes that the calling block handles garbage collection of m
    bm = bmesh.new()
    bm.from_mesh(m)
    verts = []
    for v in bm.verts:
        if (len(v.link_edges) > 1):
            verts.append(v)
    bmesh.ops.smooth_vert(bm, verts = verts, factor = 0.5)
    bm.to_mesh(m)
    bm.free()

##################################################################################
# Ribbon Mesh
# ##################################################################################
# def DetectRibbonsDEPRECATED(m, fReport = None):
#     # Returns list of vertex indices belonging to ribbon mesh edges
#     # NOTE: this assumes a mesh object with only ribbon meshes
#     # ---DO NOT call this script with a mesh that isn't a ribbon!--- #
#     #I think the initial implementation is O(n^3)
#     if ((len(m.vertices) % 2 != 0)):
#         #add more robust checks maybe
#         if (fReport):
#             fReport(type = {"ERROR_INVALID_INPUT"}, message = "Not a ribbon")
#         return None #not a  ribbon
#     tEdges, bEdges, circle, tEdge, bEdge = [],[],[],[],[],
#     iPrev = 0; jPrev = 0
#     for ind in range(len(m.vertices) //2 ):
#         i = (ind*2); j = i+1
#         circles = 0
#         if ((len(tEdge) > 5) and (len(bEdge) > 5) ):
#             #otherwise it's too small to have a circle
#             for e in m.edges:
#                 v1 = e.vertices[0]; v2 = e.vertices[1]
#                 if ( ( (i == v1) and (v2 == tEdge[0]) ) or
#                      ( (i == v2) and (v1 == tEdge[0]) ) ):
#                     circles+=1
#                 if ( ( (j == v1) and (v2 == bEdge[0]) ) or
#                      ( (j == v2) and (v1 == bEdge[0]) ) ):
#                     circles+=1
#                 if (circles == 2):
#                     break
#         if (circles == 0) and (iPrev !=0) and (jPrev !=0):
#             Gap = True
#             for e in m.edges:
#                 if ( ( (i == e.vertices[0]) and (iPrev == e.vertices[1]) ) or 
#                      ( (i == e.vertices[1]) and (iPrev == e.vertices[0]) ) ):
#                     Gap = False
#                     break
#                 if ( ( (j == e.vertices[0]) and (jPrev == e.vertices[1]) ) or 
#                      ( (j == e.vertices[1]) and (jPrev == e.vertices[0]) ) ):
#                     Gap = False
#                     break
#             if (Gap):
#                 tEdges.append( tEdge.copy() ); bEdges.append( bEdge.copy() ); circle.append(False)
#                 tEdge, bEdge = [], [] 
#                 iPrev = 0; jPrev = 0
#             else:
#                 iPrev = i; jPrev = j
#         elif (circles == 1):
#             if (fReport):
#                 fReport(type = {"ERROR_INVALID_INPUT"}, message = "Not a ribbon")
#             return None #not a  ribbon
#             #this won't actually happen, we're iterating by 2's
#             #but it's OK to check, since this should never happen anyway
#         tEdge.append(i); bEdge.append(j); iPrev = i; jPrev = j
#         if (circles == 2):
#             tEdges.append( tEdge.copy() ); bEdges.append( bEdge.copy() ); circle.append(True)
#             tEdge, bEdge = [], []
#             iPrev = 0; jPrev = 0
#     if (len(tEdge) > 0):
#         tEdges.append(tEdge); bEdges.append(bEdge); circle.append(False)
#     ribbons = []
#     for tE, bE, c in zip(tEdges, bEdges, circle):
#         ribbons.append((tE, bE, c))
#         # print ("Ribbon of length: ", len(tEdges))
#         # print (tE, bE, c) # seems a bit wasteful to store
#         #                   #  the circle every time... 
#         #                   #  but it's just a bool.
#     return ribbons

def DetectRibbons(m, fReport = None):
    # Returns list of vertex indices belonging to ribbon mesh edges
    # NOTE: this assumes a mesh object with only ribbon meshes
    # ---DO NOT call this script with a mesh that isn't a ribbon!--- #
    bm = bmesh.new()
    bm.from_mesh(m)
    mIslands, mIsland = [], []
    skipMe = set()
    bm.faces.ensure_lookup_table()
    #first, get a list of mesh islands
    for f in bm.faces:
        if (f.index in skipMe):
            continue #already done here
        checkMe = [f]
        while (len(checkMe) > 0):
            facesFound = 0
            for f in checkMe:
                if (f.index in skipMe):
                    continue #already done here
                mIsland.append(f)
                skipMe.add(f.index)
                for e in f.edges:
                    checkMe += e.link_faces
            if (facesFound == 0):
                #this is the last iteration
                mIslands.append(mIsland)
                checkMe, mIsland = [], []
    ribbons = []
    skipMe = set() # to store ends already checked
    for mIsl in mIslands:
        ribbon = None
        first = float('inf')
        for f in mIsl:
            if (f.index in skipMe):
                continue #already done here
            if (f.index < first):
                first = f.index
            adjF = 0
            for e in f.edges:
                adjF+= (len(e.link_faces) - 1)
                # every face other than this one is added to the list
            if (adjF == 1):
                ribbon = (DetectRibbon(f, bm, skipMe) )
                break
        if (ribbon == None):
            ribbon = (DetectRibbon(bm.faces[first], bm, skipMe) )
        ribbons.append(ribbon)
    # print (ribbons)
    return ribbons

def DetectRibbon(f, bm, skipMe):
    fFirst = f.index
    cont = True
    circle = False
    tEdge, bEdge = [],[]
    while (cont == True):
        skipMe.add(f.index)
        tEdge.append (f.loops[0].vert.index) # top-left
        bEdge.append (f.loops[3].vert.index) # bottom-left
        nEdge = bm.edges.get([f.loops[1].vert, f.loops[2].vert])
        nFaces = nEdge.link_faces
        if (len(nFaces) == 1): 
            cont = False
        else:
            for nFace in nFaces:
                if (nFace != f):
                    f = nFace
                    break
            if (f.index == fFirst):
                cont = False
                circle = True
        if (cont == False): # we've reached the end, get the last two:
            tEdge.append (f.loops[1].vert.index) # top-right
            bEdge.append (f.loops[2].vert.index) # bottom-right
            # this will create a loop for rings -- 
            #  "the first shall be the last and the last shall be first"
    return (tEdge,bEdge,circle)

def RotateRibbonToMatchWire(ribbon, ribbonMesh, wire, wireMesh):
    if (ribbon[2] == False):
        return ribbon #don't rotate if not a circle
    tV = ribbonMesh.vertices[ribbon[0][0]]
    bV = ribbonMesh.vertices[ribbon[1][0]]
    p1 = tV.co.lerp(bV.co, 0.5)# midpoint of first ribbon segment
    rotate = None
    closest = float('inf')
    for i, index in enumerate(wire):
        p2 = wireMesh.vertices[index].co
        distance = (p2-p1).length
        if (distance < closest):
            rotate = i * -1
            closest = distance
            if (distance < 0.001): #some threshhold
                break
    rotatedTop = f_gen.rotate(ribbon[0].copy(), rotate)
    rotatedBot = f_gen.rotate(ribbon[1].copy(), rotate)
    return (rotatedBot, rotatedTop, ribbon[2])


def RibbonMeshEdgeLengths(m, ribbon):
    tE = ribbon[0]; bE = ribbon[1]; c = ribbon[2]
    lengths = []
    for i in range( len( tE ) ): #tE and bE are same length
        if (c == True):
            v1NextInd = tE[f_gen.wrap((i+1), len(tE) - 1)]
        else:
            v1NextInd = tE[f_gen.cap((i+1) , len(tE) - 1 )]
        v1 = m.vertices[tE[i]]; v1Next = m.vertices[v1NextInd]
        if (c == True):
            v2NextInd = bE[f_gen.wrap((i+1), len(bE) - 1)]
        else:
            v2NextInd = bE[f_gen.cap((i+1) , len(bE) - 1 )]
        v2 = m.vertices[bE[i]]; v2Next = m.vertices[v2NextInd]
        
        v = v1.co.lerp(v2.co, 0.5); vNext = v1Next.co.lerp(v2Next.co, 0.5)
        # get the center, edges may not be straight so total length 
        #  of one edge may be more than the ribbon center's length
        lengths.append(( v - vNext ).length)
    return lengths


def SetRibbonData(m, ribbon):
    #maybe this could be incorporated into the DetectWireEdges function?
    #maybe I can check for closed poly curves here? under what other circumstance
    # will I find the ends of the wire have identical coordinates?
    ribbonData = []
    tE = ribbon[0].copy(); bE = ribbon[1].copy()# circle = ribbon[2]
    #
    lengths = RibbonMeshEdgeLengths(m, ribbon)
    lengths.append(0)
    totalLength = sum(lengths)
    m.calc_normals() #calculate normals
    for i, (t, b) in enumerate(zip(tE, bE)):
        ind = f_gen.wrap( (i + 1), len(tE) - 1 )
        tNext = tE[ind]; bNext = bE[ind]
        ribbonData.append(  ( (t,b), (tNext, bNext), lengths[i] ) )
        #if this is a circle, the last v in vertData has a length, otherwise 0
    return ribbonData, totalLength

def DataFromRibbon(m, factorsList, obLoc, ribbons = None, fReport = None):
    #Note, factors list should be equal in length the the number of wires
    #Now working for multiple wires, ugly tho
    if (ribbons == None):
        ribbons = DetectRibbons(m, fReport=fReport)
        if (ribbons is None):
            print ("No ribbon to get data from.")
            if (fReport):
                fReport(type = {'ERROR'}, message="No ribbon to get data from.")
            return None
    ret = []
    for factors, ribbon in zip(factorsList, ribbons):
        points  = []
        widths  = []
        normals = []
        ribbonData, totalLength = SetRibbonData(m, ribbon)

        for fac in factors:
            if (fac == 0):
                data = ribbonData[0]
                curFac = 0
            elif (fac == 1):
                data = ribbonData[-1]
                curFac = 0
            else:
                targetLength = totalLength * fac
                data = ribbonData[0]
                curLength = 0
                for ( (t, b), (tNext, bNext), length,) in ribbonData:
                    if (curLength >= targetLength):
                        break
                    curLength += length
                    data = ( (t, b), (tNext, bNext), length,)
                targetLengthAtEdge = (curLength - targetLength)
                if (targetLength == 0):
                    curFac = 0
                elif (targetLength == totalLength):
                    curFac = 1
                else:
                    try:
                        curFac = 1 - (targetLengthAtEdge/ data[2]) #length
                    except ZeroDivisionError:
                        curFac = 0
                        print ("DIVISION BY ZERO!!!")
                        #TODO use fReport here
                        #Ugly, but OK for now
            t1 = m.vertices[data[0][0]]; b1 = m.vertices[data[0][1]]
            t2 = m.vertices[data[1][0]]; b2 = m.vertices[data[1][1]]
            #location
            loc1 = (t1.co).lerp(b1.co, 0.5)
            loc2 = (t2.co).lerp(b2.co, 0.5)
            #width
            w1 = (t1.co - b1.co).length/2
            w2 = (t2.co - b2.co).length/2 #radius, not diameter
            #normal
            n1 = (t1.normal).lerp(b1.normal, 0.5)
            n2 = (t1.normal).lerp(b2.normal, 0.5)
            if ((data[0][0] > data[1][0]) and (ribbon[2] == False)):
                curFac = 0
                #don't interpolate if at the end of a ribbon that isn't circular
            if ( 0 < curFac < 1):
                outPoint = loc1.lerp(loc2, curFac)
                outNorm  = n1.lerp(n2, curFac)
                outWidth = w1 + ( (w2-w1) * curFac)
            elif (curFac <= 0):
                outPoint = loc1.copy()
                outNorm = n1
                outWidth = w1
            elif (curFac >= 1):
                outPoint = loc2.copy()
                outNorm = n2
                outWidth = w2
            outPoint += obLoc
            outNorm.normalize()
            points.append ( outPoint.copy() ) #copy because this is an actual vertex location
            widths.append ( outWidth )
            normals.append( outNorm )
        ret.append( (points, widths, normals) )
    return ret # this is a list of tuples containing three lists


#TODO: Try comparing the normal to find the nearest point
# def FindNearestUValueOnRibbonMesh(m, pointsList, fReport):
#     ribbons = DetectRibbons(m, fReport=fReport)
#     ret = []
#     # prevFactor = None
#     if (ribbons is None):
#         print ("No ribbon to get data from.")
#         if (fReport):
#             fReport(type = {'ERROR'}, message="No ribbon to get data from.")
#     for ribbon, points in zip(ribbons, pointsList):
#         ribbonData, total_length = SetRibbonData(m, ribbon)
#         factorsOut = []
#         for p in points:
#             prevDist = float('inf')
#             curDist  = float('inf')
#             data1 = None
#             data2 = None
#             for i in range(len(ribbonData) - 1):
#                 #but it shouldn't check the last one
#                 t1, b1 = ribbonData[i][0]
#                 t2, b2 = ribbonData[i][1]
#                 tV1, bV1 = m.vertices[t1], m.vertices[b1]
#                 tV2, bV2 = m.vertices[t2], m.vertices[b2]
#                 mid1 = tV1.co.lerp(bV1.co, 0.5)
#                 mid2 = tV2.co.lerp(bV2.co, 0.5)
#                 if (p == m.vertices[i].co):
#                     # but isn't vNext stored in there...?
#                     data1 = ribbonData[i]
#                     data2 = ribbonData[i+1]
#                     offset = 0
#                     break
#                 else:
#                     curDist = ( (sqrt((mid1 - p).length)) +
#                                 (sqrt((mid2 - p).length)) )/2
#                 if (curDist < prevDist):
#                     data1 = ribbonData[i]
#                     data2 = ribbonData[i+1]
#                     prevDist = curDist
#                     offset = mathutils.geometry.intersect_point_line(p, mid1, mid2)[1]
#             if (offset < 0): offset = 0
#             elif (offset > 1): offset = 1
#             # Assume the vertices are in order
#             v1Length = 0
#             v2Length = data2[2]
#             for i in range(data1[0]):
#                 v1Length += ribbonData[i][2]
#             factor = ((offset * (v2Length)) + v1Length )/total_length
#             # if ((prevFactor is not None) and (factor < prevFactor)):
#             #     factor+=1
#             factorsOut.append(factor)
#             # prevFactor = factor
#         if (ribbonData[-1][2] == 0):
#             #this means we aren't dealing with a closed curve
#             factorsOut[0] = 0
#             factorsOut[-1] = 1 #just keep the ends in place
#         ret.append( factorsOut )
#     return ret
#     #NOTE This calculates u-values really differently than regular curves
#     #  for circular curves, because it starts counting on the first segment
#     #  (that is, the segment between te last and first cv)
#     #SOoooo for now just use the curve to calculate u-values
#     # It's redundant to have two functions, anyways.
#     # However, this could work if the RotateRibbonToMatchWire function is used.
