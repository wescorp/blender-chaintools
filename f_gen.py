#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for general functions"""
import bpy
from mathutils import Vector

################################################################################
#General Utilities
################################################################################


def GetAddonPref(prop=None):
    if (prop is None):
        return None
    try: 
        (getattr(bpy.context.preferences.addons[__package__].preferences, prop))
    except AttributeError:
        if (prop == "defaultSelectionMethod"):
            #kind of a HACK while I haven't got this working. It's not important anyway
            return {'NAME', 'LOCATION'}
        return None #ChainTools is initializing

def rotate(l, n):
    if ( not ( isinstance(n, int) ) ): #print an error if n is not an int:
        raise TypeError("List slice must be an int, not float.")
    return l[n:] + l[:n]
#from stack exchange, thanks YXD

#Lazy way to do this
def cap(val, maxValue):
    if (val > maxValue):
        return maxValue
    return val

#Lazy way to do this:
def capMin(val, minValue):
    if (val < minValue):
        return minValue
    return val

#Lazy way to do this
def wrap(val, maxValue, minValue = None):
    if (val > maxValue):
        return (-1 * ((maxValue - val) + 1))
    if ((minValue) and (val < minValue)):
        return (val + maxValue)
    return val
    #TODO clean this up


def layerMaskCompare(mask_a, mask_b):
    compare = 0
    for a, b in zip(mask_a, mask_b):
        if (a != b):
            compare+=1
    if (compare == 0):
        return True
    return False

def lerpVal(a, b, fac = 0.5):
    return a + ( (b-a) * fac)
    #TODO Replace other lerps with this function.
    #     Not a priority but good for consistency.

################################################################################
# Geometry Utilities
################################################################################

def BoundingBoxFromPoints(points):
    """Takes a list of points, returns a tuple containing the corner points
       containting theminimum and maximum values in x, y, z directions"""
    xMin, yMin, zMin = float( 'inf'),float( 'inf'),float( 'inf')
    xMax, yMax, zMax = float('-inf'),float('-inf'),float('-inf')
    for p in points:
        if (p.x < xMin):
            xMin = p.x
        if (p.y < yMin):
            yMin = p.y
        if (p.z < zMin):
            zMin = p.z
        if (p.x > xMax):
            xMax = p.x
        if (p.y > yMax):
            yMax = p.y
        if (p.z > zMax):
            zMax = p.z
    return ( Vector((xMin,yMin,zMin)), Vector((xMax, yMax, zMax)) )

def BoundingBoxCenter(bb):
    """Takes a tuple containing a pair of point representing the corners of a
       Bounding Box, returns the midpoint the line between the two points."""
    return ( bb[0].lerp(bb[1], 0.5) )

def CoincidentPoints(p1, p2, tolerance = 0.01):
    if (tolerance == 0):
        return (p1 == p2)
    #a function to check if two points are coincident, with threshold
    #From https://stackoverflow.com/questions/41270792/check-for-coincident-points-within-tolerance#41270863
    # thanks kmaork, Rockcat
    same = lambda p1, p2, tol: sum((p2[i] - p1[i])**2 for i in range(len(p1))) <= tol**2
    return same(p1, p2, tolerance)

def uValuesInPoints(points):
    lengths = []
    lenTotal = 0
    for i in (range(len(points) - 1)):
        length = (points[i] - points[i+1]).length
        lengths.append(lenTotal)
        lenTotal+=length
    if (lenTotal > 0):
        returnMe = []
        for l in lengths:
            returnMe.append(l/lenTotal)
        returnMe.append(1)
        return returnMe #not sure why but returning lengths doesn't work
    else:
        return [0]