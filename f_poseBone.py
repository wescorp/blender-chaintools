#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for Pose-Mode only functions"""
import bpy

def RetargetConstraints(obArm, bonePairs):
    for pb in obArm.pose.bones:
        for c in pb.constraints:
            for (bName, sName) in bonePairs:
                bPB = obArm.pose.bones[bName]
                sPB = obArm.pose.bones[sName]
                try:
                    if (c.subtarget == bPB.name):
                        c.subtarget = sPB.name
                except: #constraint doens't have subtarget
                    pass

def MoveConstraints(obArm, bonePairs):
    for (bName, sName) in bonePairs:
        bPB = obArm.pose.bones[bName]
        sPB = obArm.pose.bones[sName]
        removeMe = []
        for c in bPB.constraints:
            CopyConstraint(sPB, c)
            removeMe.append(c)
        for c in removeMe:
            bPB.constraints.remove(c)

def CopyConstraint(pb, c):
    #from https://blender.stackexchange.com/questions/41709/how-to-copy-constraints-from-one-bone-to-another
    #thanks TLousky
    nc = pb.constraints.new( c.type )
    for prop in dir( c ):
        try:
            setattr( nc, prop, getattr( c, prop ) )
        except:
            pass